.. dummy documentation master file, Reliability problems repository.

Repository and Black-box reliability challenge 2019
===================================================

This website contains a series of benchmark problems for testing reliability analysis methods. It consists of two main parts:

* A repository for the Black-box Reliability Challenge 2019;
* A reliability problem repository for benchmarking structural reliability method.

.. note::
   **We are looking for volunteers** to organize the next reliability challenge. If you are interested to lead or participate in the organization, have suggestions on how to improve and extend the challenge, and/or would like to donate reliability problems, please contact us (|email|).

.. note::
   The challenge is closed (see the :ref:`sec:results`) but you can still use the server. If you have any questions or suggestions feel free to contact us via |email| or |Git Issues|.

.. |email| raw:: html

   <a href="mailto:arpad.rozsas@tno.nl">email</a>

.. toctree::
   :maxdepth: 1
   :caption: Challenge 2019

   introduction
   results
   participation
   quick_start
   server_interaction
   challenge2019_problems
   challenge_details
   faq

.. toctree::
   :maxdepth: 1
   :caption: Repository
   
   reliability_problems
   distributions
   references
   contributors
   contact

.. _sec:citation:

Citation
------------------------

If you use some code or results from this website please cite us:

    Rozsas A., Slobbe A. (2019). *Repository and Black-box Reliability Challenge 2019*. https://gitlab.com/rozsasarpi/rprepo/.

Bug Reports & Questions
------------------------

This project is MIT-licensed and the source is available on |GitLab|. If any questions or issues arise as you use it, please get in touch via |Git Issues|. If you wish to contribute, you can do it directly to the |GitLab| repository following the standard git procedure.

.. |GitLab| raw:: html

	<a href="https://gitlab.com/rozsasarpi/rprepo" target="_blank">GitLab</a>

.. |Git Issues| raw:: html

	<a href="https://gitlab.com/rozsasarpi/rprepo/issues" target="_blank">Git Issues</a>


:ref:`sec:sponsors`
-----------------------

.. image:: _static/tno_logo.png
   :height: 50px
   :alt: TNO

.. image:: _static/diana_logo.png
   :height: 100px
   :alt: Diana

Indices and tables
-----------------------

* :ref:`genindex`
* :ref:`search`

