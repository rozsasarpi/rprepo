
.. _sec:quick_start:

*************************
Quick start
*************************

This section walks you through the steps of evaluating performance functions and submitting results within the challenge framework.

Prerequisites:
 - internet connection;
 - basic |Matlab| or |Python| knowledge.

.. |Matlab| raw:: html

   <a href="https://www.mathworks.com/products/matlab.html" target="_blank">Matlab</a>

.. |Python| raw:: html

   <a href="https://www.python.org/" target="_blank">Python</a>


We provide a :ref:`sec:tutorial_python` and a :ref:`sec:tutorial_matlab` tutorial as well, pick the one you prefer.

.. _sec:tutorial_python:

=========================
Python
=========================

The most important actions you have to be familiar with to successfully participate in the challenge are:

- :ref:`sec:evaluate_python`, and
- :ref:`sec:submit_python`.

:ref:`sec:evaluate_python` evaluates a performance function on the web server, while :ref:`sec:submit_python` submits your intermediate and/or final results such as reliability indices and sensitivity factors to the server. Note that is of utmost importance that you also submit your intermediate results so we can compare the rate of convergence of different methods.

You need to have a Python installation (either 2.x or 3.x will work).

.. _sec:evaluate_python:

-------------------------
Evaluate
-------------------------

Download our |evaluate.py| file that manages the communication with the server and put into a folder of your choice.

.. Warning::
	GitLab will propose a filename that contains the path, rename it to ``evaluate.py``.

Set the working directory of Python to this folder and open a new py-file (script) [*]_. If you do not have the |requests| package installed (``pip install requests``) then install it as the Python files rely on it.

.. |evaluate.py| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/evaluate.py" target="_blank">evaluate.py</a>

.. |requests| raw:: html

   <a href="http://docs.python-requests.org/en/master/" target="_blank">requests</a>

First, we need to provide our ``username`` and ``password``. Once the challenge is open (:ref:`sec:timeline`) you can register and use your username and password to access the performance functions. For testing purposes let's use a predefined, test user account:

.. code-block:: python

	username = 'testuser'
	password = 'testpass'

Then we need some ids to uniquely identify our performance function. Let's pick the second problem from the :ref:`tab:tutorial_set`:

.. code-block:: python

	set_id = -1
	problem_id  = 2

Problem 2 is corresponding to :ref:`sec:rp_22` that is a two-dimensional, nonlinear function.

The next step is to specify the location where we would like to evaluate the performance function. The location is the independent variable of the function and we define it as a list with numerical items (you can also use a numpy array and bundle (vectorized) calls are also possible :ref:`sec:evaluate`):

.. code-block:: python

	x = [0.545, 1.23]

Now we have everything to evaluate the performance function. First we need to import the earlier downloaded ``evaluate.py`` function:


.. code-block:: python

	from evaluate import evaluate

Then we can use it to evaluate the performance function:

.. code-block:: python

	g_val_sys, g_val_comp, msg = evaluate(username, password, set_id, problem_id, x)
	print(msg)
	print(g_val_sys)
	print(g_val_comp)

Where ``g_val_sys`` is the performance function value on system level and ``g_val_comp`` contains a performance function value for each component involved in the problem. For cases with a single performance function (as this one) ``g_val_sys=g_val_comp``.
Executing the above code prints the following to the command window:

.. code-block:: python

  Ok
  1.2918
  [1.2918]

Congratulations! You just evaluated a performance function on our server! Go ahead and test the function with other inputs, incorrect input, and try our other functions as well.

For using the performance function in your reliability algorithm you can wrap the above code into a function or you can create a lambda function that fits your reliability algorithm, e.g.

.. code-block:: python

	def g_fun(x):
		g = evaluate(username, password, set_id, problem_id, x)
		return g

Given that within this challenge we intentionally cap your number of performance function evaluations you might wonder how many evaluations you have left. You can check this by clicking on this |link|. The ``request_limit`` is set to -1 for this tutorial example which means that there is no upper limit so you can experiment with these functions. However, for the problems included in the challenge finite limits are enforced. Once you registered you will have your dedicated page to check your submissions.

.. [*] You can download a file with all the code above from the GitLab repository: |tutorial_evaluate.py|.

.. |tutorial_evaluate.py| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/tutorial_evaluate.py" target="_blank">tutorial_evaluate.py</a>

.. |link| raw:: html

   <a href="https://tno-black-box-challenge-api.herokuapp.com/show-my-budget?user_id=testid" target="_blank">link</a>

.. _sec:submit_python:

-------------------------
Submit
-------------------------

After evaluating a performance function multiple times you might want to submit your intermediate or final results to the server to be compared with other participants. This section shows you how to do that. Note that you have to submit at least one result for a problem to be considered among the participants. Results submitted via other means than that described below are not accepted. You are strongly encouraged to submit your intermediate results as well since that will give us an idea about the rate of convergence of your method.

For submitting result you need to download the |submit.py| file. Save it into your working directory. For illustrative purposes let's make a dummy a submission with the following input (you can use numpy arrays as well):

.. code-block:: python

  from submit import submit
   
  username    = 'testuser'
  password    = 'testpass'
  set_id      = -1
  problem_id  = 2
  beta_sys    = 3.4
  beta_comp   = 3.4
  alpha_sys   = []
  alpha_comp  = [0.64, 0.77]

You have to identify yourself and specify the particular set and problem you are submitting the results to. Note that some inputs are optional, e.g. ``alpha_sys`` is left empty in this case. The results can submitted by using the ``submit`` function:

.. code-block:: python

    msg = submit(username, password, set_id, problem_id, beta_sys, beta_comp, alpha_sys, alpha_comp)

    print(msg)

To check your submission online, in the database of the web server click on this |submissions_link|.

.. |submit.py| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/submit.py" target="_blank">submit.py</a>

.. _sec:tutorial_matlab:

=========================
Matlab
=========================

The most important actions you have to be familiar with to successfully participate in the challenge are:

- :ref:`sec:evaluate_python`, and
- :ref:`sec:submit_python`.

:ref:`sec:evaluate_matlab` evaluates a performance function on the web server, while :ref:`sec:submit_matlab` submits your intermediate and/or final results such as reliability indices and sensitivity factors to the server. Note that is of utmost importance that you also submit your intermediate results so we can compare the rate of convergence of different methods.

You need to have a Matlab installation (version R2015a or above).

.. _sec:evaluate_matlab:

-------------------------
Evaluate
-------------------------

Download our |evaluate.m| file that manages the communication with the server and |parse_json.m| that is used to post-process the response from the server. Put both files into a folder of your choice.

.. Warning::
	GitLab will propose a filename that contains the path, rename it to ``evaluate.m``.

Set the working directory of Matlab to this folder and open a new m-file (script) [*]_.

.. |evaluate.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/evaluate.m" target="_blank">evaluate.m</a>

.. |parse_json.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/parse_json.m" target="_blank">parse_json.m</a>

First we need to provide our ``username`` and ``password``. Once the challenge is open (:ref:`sec:timeline`) you can register and use your username and password to access the performance functions. For testing purposes let's use a predefined, test user account:

.. code-block:: matlab

	username = 'testuser';
	password = 'testpass';

Then we need some ids to uniquely identify our performance function. Let's pick the second problem from the tutorial set (see :ref:`tab:tutorial_set` for further details):

.. code-block:: matlab

	set_id = -1;
	problem_id  = 2;

Problem 2 is corresponding to :ref:`sec:rp_22` that is a two-dimensional, nonlinear function.

The next step is to specify the location where we would like to evaluate the performance function. The location is the independent variable of the function and we define it as a numerical vector (bundle (vectorized) calls are also possible by using matrices :ref:`sec:evaluate`):

.. code-block:: matlab

	x = [0.545, 1.23];

Now we have everything to evaluate the performance function for which we are going to use the earlier downloaded ``evaluate.m`` function:

.. code-block:: matlab

    [g_val_sys, g_val_comp, msg] = evaluate(username, password, set_id, problem_id, x);
    disp(msg)
    disp(g_val_sys)
    disp(g_val_comp)

Where ``g_val_sys`` is the performance function value on system level and ``g_val_comp`` contains a performance function value for each component involved in the problem. For cases with a single performance function (as this one) ``g_val_sys=g_val_comp``.
Executing the above code prints the following to the command window:

.. code-block:: matlab

   Ok
    1.2918
    1.2918

Congratulations! You just evaluated a performance function on our server! Go ahead and test the function with other inputs, incorrect input, and try our other functions as well.

For using the performance function in your reliability algorithm you can wrap the above code into a function or you can create a anonymous function that fits your reliability algorithm, e.g.

.. code-block:: matlab

	g_fun = @(x) evaluate(username, password, set_id, problem_id, x);

Given that within this challenge we intentionally cap your number of performance function evaluations you might wonder how many evaluations you have left. You can check this by clicking on this |link|. The `request_limit` is set to -1 for this tutorial example which means that there is not upper limit so you can experiment with these functions. However, for the problems included in the challenge finite limits are enforced.

.. [*] You can download a file with all the code above from the GitLab repository: |tutorial_evaluate.m|.

.. |tutorial_evaluate.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/tutorial_evaluate.m" target="_blank">tutorial_evaluate.m</a>

.. _sec:submit_matlab:

-------------------------
Submit
-------------------------

After evaluating a performance function multiple times you might want to submit your intermediate or final results to the server to be compared with other participants. This section shows you how to do that. Note that you have to submit at least one result for a problem to be considered among the participants. Results submitted via other means than that described below are not accepted. You are strongly encouraged to submit your intermediate results as well since that will give us an idea about the rate of convergence of your method.

For submitting result you need to download the |submit.m| file. Save it into your working directory. For illustrative purposes let's make a dummy a submission with the following input:

.. code-block:: matlab

    username    = 'testuser';
    password    = 'testpass';
    set_id      = -1;
    problem_id  = 2;
    beta_sys    = 3.4;
    beta_comp   = 3.4;
    alpha_sys   = [];
    alpha_comp  = [0.64, 0.77];

You have to identify yourself and specify the particular set and problem you are submitting the results to. Note that some inputs are optional, e.g. ``alpha_sys`` is left empty in this case. The results can submitted by using the ``submit`` function:

.. code-block:: matlab

    msg = submit(username, password, set_id, problem_id, beta_sys, beta_comp, alpha_sys, alpha_comp);

    disp(msg)

To check your submission online, in the database of the web server click on this |submissions_link|.

.. |submissions_link| raw:: html

   <a href="https://tno-black-box-challenge-api.herokuapp.com/show-my-submissions?user_id=testid&set_id=-1&problem_id=2" target="_blank">link</a>

.. |submit.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/submit.m" target="_blank">submit.m</a>
