.. _sec:reliproblems:

*************************
Reliability problems
*************************

The structural reliability benchmarking framework is an on-line documentation platform that contains all kind of reliability problems with their descriptions, implementation files, and visualization of the performance functions. By making this information publicly available, it is intended to stimulate a systematic and reproducible assessment of the accuracy and efficiency of currently available reliability methods and those to come in the future. Furthermore, the platform is envisaged as a dynamic environment in which you are encouraged to contribute and add new, interesting problems.

.. note::
   For most problems the failure probabilities are calculated with numerical methods that converge to the exact solution. Due to the applied stopping criteria of these numerical methods you may attain slightly different results. The difference should be a small difference in the second decimal of the normal form (scientific notation) representation of the failure probability. The reliability index (:math:`\beta`) is calculated from the failure probability (:math:`P_\mathrm{f}`) the following way: :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`.

.. include:: reliability_problems/rp_8.inc
.. include:: reliability_problems/rp_14.inc
.. include:: reliability_problems/rp_22.inc
.. include:: reliability_problems/rp_24.inc
.. include:: reliability_problems/rp_25.inc
.. include:: reliability_problems/rp_28.inc
.. include:: reliability_problems/rp_31.inc
.. include:: reliability_problems/rp_33.inc
.. include:: reliability_problems/rp_35.inc
.. include:: reliability_problems/rp_38.inc
.. include:: reliability_problems/rp_53.inc
.. include:: reliability_problems/rp_54.inc
.. include:: reliability_problems/rp_55.inc
.. include:: reliability_problems/rp_57.inc
.. include:: reliability_problems/rp_60.inc
.. include:: reliability_problems/rp_63.inc
.. include:: reliability_problems/rp_75.inc
.. include:: reliability_problems/rp_77.inc
.. include:: reliability_problems/rp_89.inc
.. include:: reliability_problems/rp_91.inc
.. include:: reliability_problems/rp_107.inc
.. include:: reliability_problems/rp_110.inc
.. include:: reliability_problems/rp_111.inc
.. include:: reliability_problems/rp_201.inc
.. include:: reliability_problems/rp_202.inc
.. include:: reliability_problems/rp_203.inc
.. include:: reliability_problems/rp_213.inc
.. include:: reliability_problems/rp_300.inc
.. include:: reliability_problems/rp_301.inc
