
.. _sec:server_interaction:

*************************
Server interaction
*************************

This section summarizes how to interact with the server. It serves two purposes:

- a cheatsheet like refresher of the functionalities and their usage;
- a detailed description of the functionalities.

The server code is MIT-licensed and the source is available from |GitLab|.

.. |GitLab| raw:: html

  <a href="https://gitlab.com/rozsasarpi/reliability_challenge_webserver" target="_blank">GitLab</a>

=========================
Cheatsheet
=========================

Server domain:

.. code-block:: none

  https://tno-black-box-challenge-api.herokuapp.com


.. csv-table:: Action cheatsheet
    :header: "Action", "How", "Details"
    :widths: 10, 10, 10

    register, "via email, provide your name and affiliation", :ref:`sec:participation`
    ``evaluate``, ``evaluate(...).py/m``, :ref:`sec:evaluate`
    ``submit``, ``submit(...).py/m``, :ref:`sec:submit`
    show budget, ``/show-my-budget?user_id=<string>``, :ref:`sec:show_my_budget`
    show submissions, ``/show-my-submissions?user_id=<string>&set_id=<integer>&problem_id=<integer>``, :ref:`sec:show_my_submissions`

.. csv-table:: Language requirements
    :header: "Language", "Version", "Remark"
    :widths: 10, 10, 20

    Python, 2.x or 3.x, utility functions provided
    Matlab, R2015a or above, utility functions provided
    Other language, , user has to make utility functions

=========================
Details
=========================

.. _sec:evaluate:

--------------------------------------------------------
Evaluate
--------------------------------------------------------

~~~~~~~~~~~~~~~~~~
Python
~~~~~~~~~~~~~~~~~~

.. autofunction:: evaluate.evaluate

~~~~~~~~~~~~~~~~~~
Matlab
~~~~~~~~~~~~~~~~~~

See |evaluate.m| and |parse_json.m| on GitLab.

.. |evaluate.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/evaluate.m" target="_blank">evaluate.m</a>

.. |parse_json.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/parse_json.m" target="_blank">parse_json.m</a>

~~~~~~~~~~~~~~~~~~
HTTP POST request
~~~~~~~~~~~~~~~~~~

.. code-block:: none

  https://tno-black-box-challenge-api.herokuapp.com/evaluate


.. csv-table:: Evaluate, input JSON body
  :header: Key, Value, Value example
  :widths: 5, 5, 5

  ``username``, <string with quotes>, 'testuser'
  ``password``, <string with quotes>, 'testpass'
  ``set_id``, <integer>, -1
  ``problem_id``, <integer>, 2
  ``input_list``, <numbers in square bracket(s)>, "[0.545, 1.23] and [[0.545, 1.23], [0.6, 1.1]]"

``input_list`` contains values of independent variables/random variables where the performance function is evaluated. Columns are the values of random variables (x1, x2,...xn). Bundle (vectorized) call is possible by providing multiple rows, each corresponds to one set of values of the random variables.
    
.. code-block:: javascript

  {
    "username": "testuser",
    "password": "testpass",
    "set_ID": -1,
    "problem_ID": 2,
    "input_list": [0.545, 1.23]
  }

The request returns the following JSON structure:

.. csv-table:: Evaluate, output JSON body
  :header: Key, Value, Value example
  :widths: 5, 5, 5

  ``g_val_sys``, <snumbers in square bracket(s)>, [2.32]
  ``g_val_comp``, <snumbers in square bracket(s)>, "[2.32, 5.76]"
  ``msg``, <string with quotes>, 'Ok'

.. code-block:: javascript

  {
    "g_val_sys": [2.32],
    "g_val_comp": [2.32, 5.76],
    "msg": "Ok"
  }

.. raw:: html

  <hr>

.. _sec:submit:

------------------
Submit
------------------

~~~~~~~~~~~~~~~~~~
Python
~~~~~~~~~~~~~~~~~~

.. autofunction:: submit.submit

~~~~~~~~~~~~~~~~~~
Matlab
~~~~~~~~~~~~~~~~~~

See |submit.m| on GitLab.

.. |submit.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/submit.m" target="_blank">submit.m</a>
   
~~~~~~~~~~~~~~~~~~
HTTP POST request
~~~~~~~~~~~~~~~~~~

.. code-block:: none

  https://tno-black-box-challenge-api.herokuapp.com/submit


.. csv-table:: Submit, input JSON body
  :header: Key, Value, Value example
  :widths: 5, 5, 5

  ``username``, <string with quotes>, 'testuser'
  ``password``, <string with quotes>, 'testpass'
  ``set_id``, <integer>, -1
  ``beta_sys``, <number>, 3.4
  ``beta_comp``, <numbers in square bracket(s)>, "[3.4, 4.5]"
  ``alpha_sys``, <numbers in square bracket(s)>, "[]"
  ``alpha_comp``, <numbers in square bracket(s)>, "[[0.64, 0.77], [0.84, 0.54]]"

.. code-block:: javascript

  {
    "username": "testuser",
    "password": "testpass",
    "set_ID": -1,
    "problem_ID": 3,
    "beta_sys": 3.4,
    "beta_comp": [3.4, 4.5],
    "alpha_sys": [],
    "alpha_comp": [[0.64, 0.77], [0.84, 0.54]]
  }

.. raw:: html

  <hr>

.. _sec:show_my_budget:

------------------
Show budget
------------------

~~~~~~~~~~~~~~~~~~
HTTP GET request
~~~~~~~~~~~~~~~~~~

Paste the |budget_URL| to the URL bar of your browser:

.. code-block:: none

	https://tno-black-box-challenge-api.herokuapp.com/show-my-budget?user_id=testid

.. csv-table::
	:header: Key, Value, Value example
	:widths: 5, 5, 5

	``user_id``, <string without quotes>, testid

If you are registered to challenge you can use your ``user_id`` to check your consumed and available budget.

.. |budget_URL| raw:: html

   <a href="https://tno-black-box-challenge-api.herokuapp.com/show-my-budget?user_id=testid" target="_blank">URL below</a>

.. raw:: html

  <hr>

.. _sec:show_my_submissions:

------------------
Show submissions
------------------

~~~~~~~~~~~~~~~~~~
HTTP GET request
~~~~~~~~~~~~~~~~~~

The server records all requests: ``x``, ``g_val_sys``, and ``g_val_comp`` values. This makes it possible to retain earlier evaluations in case your algorithm or computer crashed in the middle of the analysis and you lost the intermediate results.

Paste the |submissions_URL| to the URL bar of your browser:

.. code-block:: none

	https://tno-black-box-challenge-api.herokuapp.com/show-my-submissions?user_id=testid&set_id=-1&problem_id=3

.. csv-table::
	:header: Key, Value, Value example
	:widths: 5, 5, 5

	``user_id``, <string without quotes>, testid
	``set_id``, <integer>, -1
	``problem_id``, <integer>, 3

If you are registered to the challenge you can use your ``user_id`` to check your submissions.

.. |submissions_URL| raw:: html

   <a href="https://tno-black-box-challenge-api.herokuapp.com/show-my-submissions?user_id=testid&set_id=-1&problem_id=3" target="_blank">URL below</a>
