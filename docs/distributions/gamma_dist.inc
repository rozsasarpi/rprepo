.. _sec:gamma:

===================================
Gamma
===================================

 
Parametrization
-----------------------------------

+-----------+------------------------------------------------------------------------------------------------------------+
| Category  | Value                                                                                                      |
+===========+============================================================================================================+
| Type      | univariate, continuous                                                                                     |
+-----------+------------------------------------------------------------------------------------------------------------+
| Support   | \ :math:`x \in (0,\infty )`\                                                                               |
+-----------+------------------------------------------------------------------------------------------------------------+
| Parameter | \ :math:`\theta_1 = k \in (0,\infty )`\, shape                                                             |
+           +------------------------------------------------------------------------------------------------------------+
|           | \ :math:`\theta_2 = \theta \in (0,\infty )`\, scale                                                        |
+-----------+------------------------------------------------------------------------------------------------------------+
| Mean      | \ :math:`k \cdot \theta`\                                                                                  |
+-----------+------------------------------------------------------------------------------------------------------------+
| Variance  | \ :math:`k \cdot \theta^2`\                                                                                |
+-----------+------------------------------------------------------------------------------------------------------------+


Probability density function
-----------------------------------

.. math::
   :label: eq:gamma_pdf

   		   f(x \mid k,\theta ) = \frac{1}{{\Gamma(k) \cdot {\theta ^k}}} \cdot {x^{k - 1}} \cdot {e^{ - \frac{x}{\theta }}}

Where \ :math:`\Gamma(.)` \ is the |gamma function|.

.. include:: distributions/gamma_pdf.plot

.. |gamma function| raw:: html

	<a href="https://en.wikipedia.org/wiki/Gamma_function" target="_blank">gamma function</a>

Cumulative distribution function
-----------------------------------

.. math::
   :label: eq:gamma_cdf

   		   F(x \mid k,\theta ) = \frac{1}{{\Gamma (k)}} \cdot \gamma \left( {k,\frac{x}{\theta }} \right)

Where \ :math:`\gamma(.)` \ is the lower |incomplete gamma function|.

.. include:: distributions/gamma_cdf.plot

.. |incomplete gamma function| raw:: html

	<a href="https://en.wikipedia.org/wiki/Incomplete_gamma_function" target="_blank">incomplete gamma function</a>

.. raw:: html

   <hr>
