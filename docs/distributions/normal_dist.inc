.. _sec:normal:

===================================
Normal
===================================

 
Parametrization
-----------------------------------

..
	.. csv-table::
	   :header: "Category", "Value"
	   :widths: 20, 20

	    "Type", 							"univariate, continuous"
		"Support", 							"\ :math:`x\in \mathbb {R}`\"
		"Parameters", 	 					"\ :math:`\mu \in \mathbb {R}`\, location" 
		"Parameters", 						"\ :math:`\sigma > 0`\, scale" 
		"Mean", 							"\ :math:`\mu`\"
		"Variance", 						"\ :math:`\sigma^2`\"

+-----------+--------------------------------------------------------------------------------------------------------------------------+
| Category  | Value                                                                                                                    |
+===========+==========================================================================================================================+
| Type      | univariate, continuous                                                                                                   |
+-----------+--------------------------------------------------------------------------------------------------------------------------+
| Support   | \ :math:`x \in (-\infty,\infty)`\                                                                                        |
+-----------+--------------------------------------------------------------------------------------------------------------------------+
| Parameter | \ :math:`\theta_1 = \mu \in (-\infty,\infty)`\, location                                                                 |
+           +--------------------------------------------------------------------------------------------------------------------------+
|           | \ :math:`\theta_2 = \sigma \in (0,\infty)`\, scale                                                                       |
+-----------+--------------------------------------------------------------------------------------------------------------------------+
| Mean      | \ :math:`\mu`\                                                                                                           |
+-----------+--------------------------------------------------------------------------------------------------------------------------+
| Variance  | \ :math:`\sigma^2`\                                                                                                      |
+-----------+--------------------------------------------------------------------------------------------------------------------------+


Probability density function
-----------------------------------

.. math::
   :label: eq:norm_pdf

   		   f(x \mid \mu ,\sigma) = {\frac {1}{\sqrt {2\pi \sigma ^{2}}}} \cdot e^{-{\frac {(x-\mu )^{2}}{2\sigma ^{2}}}} = \phi \left( {\frac{{x - \mu }}{\sigma }} \right)

.. include:: distributions/normal_pdf.plot


Cumulative distribution function
-----------------------------------

.. math::
   :label: eq:norm_cdf

   		   F(x \mid \mu ,\sigma ) = \frac{1}{2} \cdot \left( {1 + erf\left( {\frac{{x - \mu }}{{\sigma  \cdot \sqrt 2 }}} \right)} \right) = \Phi \left( {\frac{{x - \mu }}{\sigma }} \right)

.. include:: distributions/normal_cdf.plot

.. raw:: html

   <hr>