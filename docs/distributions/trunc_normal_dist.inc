.. _sec:trunc_normal:

===================================
Truncated Normal
===================================

 
Parametrization
-----------------------------------


+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Category  | Value                                                                                                                                                                                                                                                                                                                                                                                                                      |
+===========+============================================================================================================================================================================================================================================================================================================================================================================================================================+
| Type      | univariate, continuous                                                                                                                                                                                                                                                                                                                                                                                                     |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Support   | \ :math:`x \in [a,b]`                                                                                                                                                                                                                                                                                                                                                                                                      |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter | \ :math:`\theta_1 = \mu \in (-\infty,\infty)`\, location                                                                                                                                                                                                                                                                                                                                                                   |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | \ :math:`\theta_2 = \sigma \in (0,\infty)`\, scale [*]_                                                                                                                                                                                                                                                                                                                                                                    |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | \ :math:`\theta_3 = a \in (-\infty,b)`\, lower bound                                                                                                                                                                                                                                                                                                                                                                       |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | \ :math:`\theta_4 = b \in (a,\infty)`\, upper bound                                                                                                                                                                                                                                                                                                                                                                        |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Mean      | \ :math:`\mu +{\frac {\phi (\alpha )-\phi (\beta )}{Z}} \cdot \sigma`                                                                                                                                                                                                                                                                                                                                                      |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Variance  | \ :math:`{\sigma ^2} \cdot \left[ {1 + \frac{{\alpha  \cdot \phi (\alpha ) - \beta  \cdot \phi (\beta )}}{Z} - {{\left( {\frac{{\phi (\alpha ) - \phi (\beta )}}{Z}} \right)}^2}} \right]`\                                                                                                                                                                                                                                |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. [*] For a more rigorous definition see the Wikipedia page of the |truncated normal distribution|.

.. |truncated normal distribution| raw:: html

   <a href="https://en.wikipedia.org/wiki/Truncated_normal_distribution" target="_blank">truncated normal distribution</a>


Probability density function
-----------------------------------

.. math::
   :label: eq:trunc_norm_pdf

   		   f(x \mid \mu ,\sigma ,a,b) = \left\{ {\begin{array}{*{20}{l}}{\frac{{\phi (\xi )}}{{\sigma  \cdot Z}}{\mkern 1mu} }&{{\text{for }}x \in [a,b]}\\0&{{\rm{otherwise}}}\end{array}} \right.

Where the auxiliary variables are defines as:

.. math::
   :label: eq:trunc_norm_auxvar

   		   \begin{array}{l}\xi  = \frac{{x - \mu }}{\sigma },\;\alpha  = \frac{{a - \mu }}{\sigma },\;\beta  = \frac{{b - \mu}}{\sigma }\\Z = \Phi (\beta ) - \Phi (\alpha )\end{array}

and  \ :math:`\phi(.)` \ and \ :math:`\Phi(.)` \ are the standard normal probability density function (Eq. :eq:`eq:norm_pdf`) and cumulative distribution function (Eq. :eq:`eq:norm_cdf`), respectively. 

.. include:: distributions/trunc_normal_pdf.plot


Cumulative distribution function
-----------------------------------

.. math::
   :label: eq:trunc_norm_cdf

   		   F(x\mid \mu ,\sigma ,a,b) = \left\{ {\begin{array}{*{20}{l}}0&{{\text{for }}x < a}\\{\frac{{\Phi (\xi ) - \Phi (\alpha )}}{Z}}&{{\text{for }}x \in [a,b)}\\1&{{\text{for }}x \ge b}\end{array}} \right.

.. include:: distributions/trunc_normal_cdf.plot

.. raw:: html

   <hr>
