.. _sec:challenge_details:

*************************
Challenge details
*************************

======================================
Accessing performance functions
======================================

The performance functions are located on a cloud server managed by the organizers. The functions are evaluated upon a
request by the participants (:numref:`fig:framework`). The evaluation request (interfacing with the server) is a
HTTP (Hypertext Transfer Protocol) request that is the backbone of modern web development and ubiquitous in
distributed system communication [Podila2013]_.

If you are using Python or Matlab you do not have to know anything about HTTP request as we created an easy-to-use
function in each of these languages to access the performance functions on our server (see :ref:`sec:evaluate`). If you are using another language then you have to have a basic familiarity
with HTTP, see :ref:`sec:http_other_languages`.

.. _fig:framework:

.. figure:: _static/challenge_framework.png
   :scale: 75 %
   :alt: challenge framework

   High level overview of the challenge framework.


.. _sec:http_other_languages:

------------------
Other languages
------------------

If you are using a language other than Python or Matlab, or you want to make your own request directly you have to
make a HTTP POST request with a JSON body to our server. The URL for the POST request:
``https://tno-black-box-challenge-api.herokuapp.com/evaluate``. It should contain a JSON body with the following
fields (the values are taken from :ref:`sec:quick_start`):

.. code-block:: javascript

    {
       "username":'testuser',
       "password":'testpass',
       "set_ID": -1,
       "problem_ID" : 2,
       "input_list" : [0.545, 1.23]
    }

The server returns a JSON file with ``'msg'``, ``'g_val_sys'``, and ``'g_val_comp'`` fields.

======================================
The web server
======================================

The web server is hosted in the cloud by the challenge organizers and can be accessed via HTTP request. It is a |Flask| server. After the closure of the challenge of 2019 we open-sourced the server that can be pulled from |GitLab|.

.. |Flask| raw:: html

   <a href="http://flask.pocoo.org/" target="_blank">Flask</a>

   The server code is MIT-licensed and the source is available from |GitLab|.

.. |GitLab| raw:: html

  <a href="https://gitlab.com/rozsasarpi/reliability_challenge_webserver" target="_blank">GitLab</a>
