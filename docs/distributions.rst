.. _sec:distributions:

**********************************
Distributions
**********************************

* Continuous:

  * :ref:`sec:normal`
  * :ref:`sec:lognormal`
  * :ref:`sec:gumbel_max`
  * :ref:`sec:gamma`
  * :ref:`sec:uniform`
  * :ref:`sec:trunc_normal`
  * :ref:`sec:exponential`

.. include:: distributions/normal_dist.inc
.. include:: distributions/lognormal_dist.inc
.. include:: distributions/gumbel_max_dist.inc
.. include:: distributions/gamma_dist.inc
.. include:: distributions/uniform_dist.inc
.. include:: distributions/trunc_normal_dist.inc
.. include:: distributions/exponential_dist.inc