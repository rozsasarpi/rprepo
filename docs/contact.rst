.. _sec:contact:

***********************************
Contact
***********************************

If you have any questions or observations please get in touch via |email| or |Git Issues|. If you wish to contribute, you can do it directly to the |GitLab| repository following the standard git procedure.


.. |email| raw:: html

   <a href="mailto:arpad.rozsas@tno.nl">email</a>

.. |GitLab| raw:: html

	<a href="https://gitlab.com/rozsasarpi/rprepo" target="_blank">GitLab</a>

.. |Git Issues| raw:: html

	<a href="https://gitlab.com/rozsasarpi/rprepo/issues" target="_blank">Git Issues</a>