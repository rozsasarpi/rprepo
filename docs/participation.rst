.. _sec:participation:

*************************
Participation
*************************

Everyone is welcome to join the challenge. There are two ways to access the reliability problems on the challenge server:

- Without registration: using the ``testuser`` credentials given in :ref:`sec:tutorial_set`. Without registration you have access only to the tutorial problems. To get full access you need to register, see the next point.
- With registration: to get full access to the reliability problems and to participate in the challenge.

=========================
Registration
=========================

.. NOTE::
   The challenge is closed but you can still register to access the reliability problem!

You can register by sending an |email| to us expressing your intent. You will receive your ``username``, ``password``, and ``user_id`` which you can use to interact with the web server (see :ref:`sec:quick_start`).

Please keep the following in mind:

- In the email please indicate your name and affiliation (teams can also participate).
- Use the following subject line in the email: Registration Black-box Reliability Challenge 2019.
- One person is allowed to register only once (or part of only a single team).
- Do not share your results and/or performance function evaluations with other participants.

We expect you to play fair and adhere to the single registration and not sharing principles. It is in our common interest to have everyone the same information available. After the completion of the challenge we will invite the participants to share their algorithms where others can verify the results.


=========================
Contribution
=========================

We would love to hear your opinion, suggestions, and have you contributing to this project. Please do not hesitate to contact us if you:

- have any questions or issues;
- have any suggestions;
- would like to donate a reliability problem or two;
- would like to contribute to the code.

You can get in touch via |email| or |Git Issues|. You can also directly contribute to the |GitLab| repository following the standard git procedure.

.. |email| raw:: html

   <a href="mailto:arpad.rozsas@tno.nl">email</a>

.. |GitLab| raw:: html

    <a href="https://gitlab.com/rozsasarpi/rprepo" target="_blank">GitLab</a>

.. |Git Issues| raw:: html

    <a href="https://gitlab.com/rozsasarpi/rprepo/issues" target="_blank">Git Issues</a>