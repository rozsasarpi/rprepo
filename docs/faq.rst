.. _sec:faq:

****************************
Frequently asked questions
****************************

Can I submit multiple evaluation requests simultaneously?
	Yes. Bundle (vectorized) call is possible by providing multiple rows in ``input_list``, each corresponds to one set of values of the random variables. Columns are the values of random variables (:math:`x_1, x_2,\cdots x_n`). For example making three evaluation requests of a two-dimensional problem:

	- in Python: input_list = ``[[0.545, 1.23], [0.6, 1.1], [0.4, 1.6]]`` or ``numpy.array([[0.545, 1.23], [0.6, 1.1], [0.4, 1.6]])``
	- in Matlab: input_list = ``[0.545, 1.23; 0.6, 1.1; 0.4, 1.6]``

What if I'm using a programming language other than Python or Matlab?
	The challenge framework is designed in a way that you can use any programming language. If you are not using
	Python or Matlab then you have to make the HTTP request to our server yourself, see :ref:`sec:server_interaction`.

Is the web server down? I receive an error message that my request is timed out.
	You can check whether the web server is up by clicking on |web_server_link|. You should see a plain welcome screen.
	If the server is down please contact us. If the server is up please contact us with the detailed description of your problem, and the programming language and operating system you are using.

My request has timed out. What can/should I do?
	If the server is up, you can check it by clicking on |web_server_link|, then the timeout might be caused by a large number of evaluations submitted at once (in a vectorized format). Since some performance functions involve (relatively) computationally demanding numerical algorithms, it can take a while to complete all the evaluations if you submit hundreds or thousands of them at once. You can either reduce the number of requested one-time evaluations and have multiple requests, or you can increase the waiting time of your local program which makes the request:

	- in Python: Open |evaluate.py| and change the number (30 seconds) in line ``r = requests.post(main_url + 'evaluate', json=body, timeout=30)`` to a greater one.
	- in Matlab: Open |evaluate.m| and change the number (30 seconds) in line ``options.Timeout = 30;`` to a greater one.

What is the airspeed velocity of an unladen swallow?
    "Although a definitive answer would of course require further measurements, published species-wide averages of wing length and body mass, initial Strouhal estimates based on those averages and cross-species comparisons, the
    Lund wind tunnel study of birds flying at a range of speeds, and revised Strouhal numbers based on that study all lead me to estimate that the average cruising airspeed velocity of an unladen European Swallow is roughly 11 meters per second, or 24 miles an hour." [Corum2003]_

What is ``g_val_sys`` and ``g_val_comp`` and why are they equal?
	``g_val_sys`` is the performance function value on system level and ``g_val_comp`` contains a performance function value for each component involved in the problem. For cases with a single performance function value the component performance function is identical with the system performance function, ``g_val_sys=g_val_comp``.

What is ``alpha_sys`` and ``alpha_comp``?
	They are sensitivity values/factors: direction cosines of the vector pointing from the origin to the design point in U-space. These sensitivity factors are often reported when FORM analysis performed and popular in standardization. For more details see section 8.1 in [Ditlevsen2007]_, where also ``alpha`` is used to denote the same variable (sensitivity factor).


What is the ``user_id`` for?
	It can be used to:

	- Check your remaining number of performance function evaluations (:ref:`sec:show_my_budget`), e.g. |budget_example|.
	- Check your submissions (:ref:`sec:show_my_submissions`), e.g. |submission_example|.

	Change the ``user_id`` in the URLs to yours to see your budget and submissions.

Can I contribute a reliability problem?
	Sure! We would be happy to have your contribution. You can send it via |email| or add it the |GitLab| code repository directly. After the challenge has been completed the code of the web server and all the user reliability problems will be made available in a public repository.

Can I have access/download the probabilistic input for all reliability problems?
	Yes. You can download the probabilistic models from the GitLab repository in a tabular format by clicking on |Probabilistic models|.


.. |evaluate.py| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/evaluate.py" target="_blank">evaluate.py</a>

.. |evaluate.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/http_request/evaluate.m" target="_blank">evaluate.m</a>


.. |web_server_link| raw:: html

   <a href="https://tno-black-box-challenge-api.herokuapp.com" target="_blank">this link</a>


.. |budget_example| raw:: html

   <a href="https://tno-black-box-challenge-api.herokuapp.com/show-my-budget?user_id=testid" target="_blank">budget example</a>


.. |submission_example| raw:: html

   <a href="https://tno-black-box-challenge-api.herokuapp.com/show-my-submissions?user_id=testid&set_id=-1&problem_id=3" target="_blank">submission example</a>

.. |email| raw:: html

   <a href="mailto:arpad.rozsas@tno.nl">email</a>

.. |GitLab| raw:: html

	<a href="https://gitlab.com/rozsasarpi/rprepo" target="_blank">GitLab</a>   

.. |Probabilistic models| raw:: html

	<a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/misc/probabilistic_models_up.csv" target="_blank">this link</a>

