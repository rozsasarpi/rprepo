.. _sec:rp_14: 

=========================
RP14
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 1
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			5
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`7.52\cdot10^{-3}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 2.42
	"Number of performance functions", 		1
	"Reference", 							[Li2018]_



Performance function
-------------------------

.. math::
   :label: eq:rp_14

   		   \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = {X_1} - {{32} \over {\pi X_2^3}} \cdot \root 2 \of {{{X_3^2X_4^2} \over {16}} + X_5^2}   \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}

Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\", 	"NA", ":ref:`sec:uniform`",70.0,80.0,,,75.0,2.887
    "\ :math:`X_2`\", 	"NA", ":ref:`sec:normal`",39.0,0.1,,,39.0,0.1
    "\ :math:`X_3`\", 	"NA", ":ref:`sec:gumbel_max`",1342.0,272.9,,,1500.0,350.0
    "\ :math:`X_4`\", 	"NA", ":ref:`sec:normal`",400.0,0.1,,,400.0,0.1
    "\ :math:`X_5`\", 	"NA", ":ref:`sec:normal`",250000.0,35000.0,,,250000.0,35000.0

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_14_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_14.gfun_14

.. raw:: html

   <hr>
