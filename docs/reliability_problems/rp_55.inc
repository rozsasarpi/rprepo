.. _sec:rp_55: 

=========================
RP55
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 4
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`3.60\cdot10^{-1}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", -0.15
	"Number of performance functions", 		4
	"Reference", 							[Jiang2017]_



Performance function
-------------------------

.. math::
   :label: eq:rp_55

   		    \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) = 0.2 + 0.6 \cdot {({X_1} - {X_2})^4} - {{{X_1} - {X_2}} \over {\root 2 \of 2 }}  \cr
          & {g_{\mathrm{comp},2}}({\bf X}) = 0.2 + 0.6 \cdot {({X_1} - {X_2})^4} + {{{X_1} - {X_2}} \over {\root 2 \of 2 }}  \cr
          & {g_{\mathrm{comp},3}}({\bf X}) = {X_1} - {X_2} + {5 \over {\root 2 \of 2 }} - 2.2  \cr
          & {g_{\mathrm{comp},4}}({\bf X}) = {X_2} - {X_1} + {5 \over {\root 2 \of 2 }} - 2.2  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = \min \left\{ \matrix{
          {g_{\mathrm{comp},1}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},2}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},3}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},4}}({\bf X}) \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:uniform`",-1.0,1.0,,,0.0,0.5774
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:uniform`",-1.0,1.0,,,0.0,0.5774
    

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_55_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_55.gfun_55

.. raw:: html

   <hr>
