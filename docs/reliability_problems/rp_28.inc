.. _sec:rp_28: 

=========================
RP28
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 3
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`1.46\cdot10^{-7}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 5.11
	"Number of performance functions", 		1
	"Reference", 							[Grooteman2011]_



Performance function
-------------------------

.. math::
   :label: eq:rp_28

   		   \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = {X_1} \cdot {X_2} - 146.14  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",78064.0,11710.0,,,78064.0,11710.0
    "\ :math:`X_2`\",   "NA", ":ref:`sec:normal`",0.0104,0.00156,,,0.0104,0.00156

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_28_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_28.gfun_28

.. raw:: html

   <hr>
