.. _sec:rp_35:

=========================
RP35
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 3
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`3.54\cdot10^{-3}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 2.70
	"Number of performance functions", 		2
	"Reference", 							[Grooteman2011]_



Performance function
-------------------------

.. math::
   :label: eq:rp_35

   		   \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) = 2 - {X_2} + \exp ( - 0.1 \cdot X_1^2) + {(0.2 \cdot {X_1})^4}  \cr
          & {g_{\mathrm{comp},2}}({\bf X}) = 4.5 - {X_1} \cdot {X_2}  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = \min \left\{ \matrix{
          {g_{\mathrm{comp},1}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},2}}({\bf X}) \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_35_matrix.png


Implementation
-------------------------

.. autofunction:: gfun_35.gfun_35

.. raw:: html

   <hr>
