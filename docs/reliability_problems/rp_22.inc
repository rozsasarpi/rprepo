.. _sec:rp_22:

=========================
RP22
=========================

Quadratic function with mixed term, convex.

.. csv-table:: :ref:`tab:tutorial_set`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    -1, 2
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								              "symbolic"
	  "Number of random variables", 			           2
    "Failure probability, :math:`P_\mathrm{f}`", \ :math:`4.16\cdot10^{-3}`\  
	  "Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 	2.64
	  "Number of performance functions", 				   1
	  "Reference", 							            [Grooteman2011]_


Performance function
-------------------------

.. math::
   :label: eq:rp_22

   		   \eqalign{
         & {g({\bf X})} = 2.5-\frac{(X_1 + X_2)}{\sqrt{2}} + 0.1\cdot (X_1 - X_2)^2	\cr
         & {g_{\mathrm{sys}}}({\bf X}) = g_{\mathrm{comp}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_2`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_22_matrix.png

Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_22.gfun_22


Matlab
^^^^^^^^^

See |gfun_22.m| on GitLab.

.. |gfun_22.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/gfun/gfun_22.m" target="_blank">gfun_22.m</a>

.. raw:: html

   <hr>
