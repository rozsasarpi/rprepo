.. _sec:rp_77:

=========================
RP77
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 7
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			3
	"Failure probability, :math:`P_\mathrm{f}`",   \ :math:`2.87\cdot10^{-7}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 5.00
	"Number of performance functions", 		2
	"Reference", 							[Waarts2000]_



Performance function
-------------------------

.. math::
   :label: eq:rp_77

         \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = \left\{ \matrix{
          {X_1} - {X_2} - {X_3}{\rm{  , }}{X_3} \le 5 \hfill \cr
          {{\rm{X}}_3}{\rm{ - }}{{\rm{X}}_2}{\rm{             , }}{X_3} > 5{\rm{ }} \hfill \cr}  \right.  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:normal`",10.0,0.5,,,10.0,0.5
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{3}`\",   "NA", ":ref:`sec:normal`",4.0,1.0,,,4.0,1.0


The random variables are mutually independent.

Visualization
-------------------------
 
Python
^^^^^^^^^

.. image:: _static/gfun_matrix_plot/rp_77_matrix.png


Implementation
-------------------------

.. autofunction:: gfun_77.gfun_77

.. raw:: html

   <hr>
