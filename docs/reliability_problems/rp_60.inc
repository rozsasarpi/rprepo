.. _sec:rp_60:

=========================
RP60
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 6
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			5
	"Failure probability, :math:`P_\mathrm{f}`",   \ :math:`4.56\cdot10^{-2}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 1.70
	"Number of performance functions", 		7
	"Reference", 							[Wei2018]_



Performance function
-------------------------

.. math::
   :label: eq:rp_60

   		    \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) = {X_1} - {X_5}  \cr
          & {g_{\mathrm{comp},2}}({\bf X}) = {X_2} - {{{X_5}} \over 2}  \cr
          & {g_{\mathrm{comp},3}}({\bf X}) = {X_3} - {{{X_5}} \over 2}  \cr
          & {g_{\mathrm{comp},4}}({\bf X}) = {X_4} - {{{X_5}} \over 2}  \cr
          & {g_{\mathrm{comp},5}}({\bf X}) = {X_2} - {X_5}  \cr
          & {g_{\mathrm{comp},6}}({\bf X}) = {X_3} - {X_5}  \cr
          & {g_{\mathrm{comp},7}}({\bf X}) = {X_4} - {X_5}  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = \min \left\{ \matrix{
          {g_{\mathrm{comp},1}}({\bf X}) \hfill \cr
          \max \left\{ \matrix{
          \min \left\{ \matrix{
          {g_{\mathrm{comp},2}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},3}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},4}}({\bf X}) \hfill \cr}  \right. \hfill \cr
          \max \left\{ \matrix{
          \min \left\{ \matrix{
          {g_{\mathrm{comp},5}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},6}}({\bf X}) \hfill \cr}  \right. \hfill \cr
          {g_{\mathrm{comp},7}}({\bf X}) \hfill \cr}  \right. \hfill \cr}  \right. \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:lognormal`",7.691,0.09975,,,2200.0,220.0
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:lognormal`",7.645,0.09975,,,2100.0,210.0
    "\ :math:`X_{3}`\",   "NA", ":ref:`sec:lognormal`",7.736,0.09975,,,2300.0,230.0
    "\ :math:`X_{4}`\",   "NA", ":ref:`sec:lognormal`",7.596,0.09975,,,2000.0,200.0
    "\ :math:`X_{5}`\",   "NA", ":ref:`sec:lognormal`",7.016,0.3853,,,1200.0,480.0


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_60_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_60.gfun_60

.. raw:: html

   <hr>
