.. _sec:rp_107:

=========================
RP107
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 10
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			10
	"Failure probability, :math:`P_\mathrm{f}`",   \ :math:`2.92\cdot10^{-7}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 5.0
	"Number of performance functions", 		1
	"Reference", 							[Engelund1993]_



Performance function
-------------------------

.. math::
   :label: eq:rp_107

   		   \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = 5 \cdot \root 2 \of {10}  - \sum\limits_{i = 1}^{10} {{X_i}}   \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\"
    "\ :math:`X_{10}`\",  "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
 

The random variables are identically distributed and mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_107_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_107.gfun_107

.. raw:: html

   <hr>
