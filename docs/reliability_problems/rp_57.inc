.. _sec:rp_57:

=========================
RP57
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 5
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`2.84\cdot10^{-2}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 1.91
	"Number of performance functions", 		3
	"Reference", 							[Wei2018]_



Performance function
-------------------------

.. math::
   :label: eq:rp_57

   		    \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) =  - X_1^2 + X_2^3 + 3  \cr
          & {g_{\mathrm{comp},2}}({\bf X}) = 2 - {X_1} - 8 \cdot {X_2}  \cr
          & {g_{\mathrm{comp},3}}({\bf X}) = {({X_1} + 3)^2} + {({X_2} + 3)^2} - 4  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = \min \left\{ \matrix{
          \max \left\{ \matrix{
          {g_{\mathrm{comp},1}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},2}}({\bf X}) \hfill \cr}  \right. \hfill \cr
          {g_{\mathrm{comp},3}}({\bf X}) \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_57_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_57.gfun_57

.. raw:: html

   <hr>
