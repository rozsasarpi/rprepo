.. _sec:rp_24: 

=========================
RP24
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 2
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`2.86\cdot10^{-3}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 2.76
	"Number of performance functions", 		1
	"Reference", 							[Grooteman2011]_



Performance function
-------------------------

.. math::
   :label: eq:rp_24

   		    \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = 2.5 - 0.2357 \cdot ({X_1} - {X_2}) + 0.00463 \cdot {({X_1} + {X_2} - 20)^4}  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",10.0,3.0,,,10.0,3.0
    "\ :math:`X_2`\",   "NA", ":ref:`sec:normal`",10.0,3.0,,,10.0,3.0

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_24_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_24.gfun_24

.. raw:: html

   <hr>
