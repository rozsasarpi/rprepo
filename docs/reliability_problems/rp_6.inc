.. _sec:rp_6:

=========================
RP6
=========================


 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								DACC
	"Number of random variables", 			6
	"Failure probability, :math:`P_\mathrm{f}`",  DACC	
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", DACC
	"Number of performance functions", 				DACC
	"Continuity", 							DACC
	"Reference", 							DACC



Performance function
-------------------------

.. math::
   :label: eq:rp_6

   		   DACC


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\", 	"NA", ":ref:`sec:normal`", 0.5, 0.05
	"\ :math:`X_2`\", 	"NA", ":ref:`sec:normal`", 0.45, 0.0725
	"\ :math:`X_3`\", 	"NA", ":ref:`sec:normal`", 1, 0.05
	"\ :math:`X_4`\", 	"NA", ":ref:`sec:normal`", 1, 0.1
	"\ :math:`X_5`\", 	"NA", ":ref:`sec:normal`", 0.1, 0.01
	"\ :math:`X_6`\", 	"NA", ":ref:`sec:normal`", 1, 0.2
	
The random variables are mutually independent.

Visualization
-------------------------
 
DACC


Implementation
-------------------------

DACC

.. raw:: html

   <hr>
