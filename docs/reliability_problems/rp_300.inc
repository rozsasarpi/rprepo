.. _sec:rp_300: 

=========================
RP300
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 15
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Numerical
	"Number of random variables", 			14
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`5.22\cdot10^{-5}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.88
	"Number of performance functions", 		1
	"Reference", 							[Sepulveda2019]_



Performance function
--------------------

Too complex to be written here, see the implementation in the code repository.


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\", 	"DACC", ":ref:`sec:normal`", 0, 1
	"\ :math:`X_2`\", 	"DACC", ":ref:`sec:normal`", 0, 1
	"\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\"
	"\ :math:`X_{14}`\", 	"DACC", ":ref:`sec:normal`", 0, 1

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_300_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_300_juan_first_passage.gfun_300

.. raw:: html

   <hr>
