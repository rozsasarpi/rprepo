.. _sec:rp_53:

=========================
RP53
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 6
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`3.13\cdot10^{-2}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 1.86
	"Number of performance functions", 		1
	"Reference", 							[Xiao2018]_



Performance function
-------------------------

.. math::
   :label: eq:rp_53

   		    \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = \sin \left( {{{5 \cdot {X_1}} \over 2}} \right) + 2 - {{(X_1^2 + 4) \cdot ({X_2} - 1)} \over {20}}  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",1.5,1.0,,,1.5,1.0
    "\ :math:`X_2`\",   "NA", ":ref:`sec:normal`",2.5,1.0,,,2.5,1.0
 


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_53_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_53.gfun_53

.. raw:: html

   <hr>
