.. _sec:rp_301: 

=========================
RP301
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 16
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Numerical (finite element analysis surrogate)
	"Number of random variables", 			12
	"Failure probability, :math:`P_\mathrm{f}`", \ :math:`6.9\cdot10^{-5}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.81
	"Number of performance functions", 		1
	"Reference", 							[Slobbe2019]_



Performance function
--------------------

Too complex to be written here, see the implementation in the code repository.



Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:lognormal`",4.476,0.05677,,,88.0,5.0
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:lognormal`",6.084,0.0681,,,440.0,30.0
    "\ :math:`X_{3}`\",   "NA", ":ref:`sec:lognormal`",6.18,0.0681,,,484.0,33.0
    "\ :math:`X_{4}`\",   "NA", ":ref:`sec:lognormal`",-2.663,0.08982,,,0.07,0.0063
    "\ :math:`X_{5}`\",   "NA", ":ref:`sec:lognormal`",6.378,0.06941,,,590.0,41.0
    "\ :math:`X_{6}`\",   "NA", ":ref:`sec:lognormal`",6.473,0.06925,,,649.0,45.0
    "\ :math:`X_{7}`\",   "NA", ":ref:`sec:lognormal`",-2.663,0.08982,,,0.07,0.0063
    "\ :math:`X_{8}`\",   "NA", ":ref:`sec:normal`",590.0,59.0,,,590.0,59.0
    "\ :math:`X_{9}`\",   "NA", ":ref:`sec:gumbel_max`",268.4,46.0,,,295.0,59.0
    "\ :math:`X_{10}`\",  "NA", ":ref:`sec:lognormal`",0.03797,0.04997,,,1.04,0.052
    "\ :math:`X_{11}`\",  "NA", ":ref:`sec:lognormal`",-0.004975,0.09975,,,1.0,0.1
    "\ :math:`X_{12}`\",  "NA", ":ref:`sec:lognormal`",-0.004975,0.09975,,,1.0,0.1

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_301_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_301.gfun_301

.. raw:: html

   <hr>
