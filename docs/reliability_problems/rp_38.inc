.. _sec:rp_38:

=========================
RP38
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 5
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			7\
	"Failure probability, :math:`P_\mathrm{f}`", \ :math:`8.10\cdot10^{-3}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 2.48
	"Number of performance functions", 		1
	"Reference", 							[Yun2018]_



Performance function
-------------------------

.. math::
   :label: eq:rp_38


   		    \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = 15.59 \cdot {10^4} - {{{X_1} \cdot X_2^3} \over {2 \cdot X_2^3}} \cdot {{X_4^2 - 4 \cdot {X_5} \cdot {X_6} \cdot X_7^2 + {X_4} \cdot ({X_6} + 4 \cdot {X_5} + 2 \cdot {X_6} \cdot {X_7})} \over {{X_4} \cdot {X_5} \cdot ({X_4} + {X_6} + 2 \cdot {X_6} \cdot {X_7})}}  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",350.0,35.0,,,350.0,35.0
    "\ :math:`X_2`\",   "NA", ":ref:`sec:normal`",50.8,5.08,,,50.8,5.08
    "\ :math:`X_3`\",   "NA", ":ref:`sec:normal`",3.81,0.381,,,3.81,0.381
    "\ :math:`X_4`\",   "NA", ":ref:`sec:normal`",173.0,17.3,,,173.0,17.3
    "\ :math:`X_5`\",   "NA", ":ref:`sec:normal`",9.38,0.938,,,9.38,0.938
    "\ :math:`X_6`\",   "NA", ":ref:`sec:normal`",33.1,3.31,,,33.1,3.31
    "\ :math:`X_7`\",   "NA", ":ref:`sec:normal`",0.036,0.0036,,,0.036,0.0036


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_38_matrix.png


Implementation
-------------------------

.. autofunction:: gfun_38.gfun_38

.. raw:: html

   <hr>
