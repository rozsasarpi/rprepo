.. _sec:rp_8:

=========================
RP8
=========================

Six-dimensional hyperplane.

.. csv-table:: :ref:`tab:tutorial_set`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    -1, 1


Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								"symbolic"
	"Number of random variables", 			6
	"Failure probability, :math:`P_\mathrm{f}`", \ :math:`7.84\cdot10^{-4}`\	
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.16
	"Number of performance functions", 		1
	"Reference", 							[Xu2018]_



Performance function
-------------------------

.. math::
   :label: eq:rp_8

   		   \eqalign{
   		   & {g_{\mathrm{comp}}}({\bf X}) = X_1 + 2X_2 + 2X_3 + X_4 - 5X_5 - 5X_6 \cr
         & {g_{\mathrm{sys}}}({\bf X}) = g_{\mathrm{comp}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\", 	"NA", ":ref:`sec:lognormal`",4.783,0.09975,,,120.0,12.0
    "\ :math:`X_2`\", 	"NA", ":ref:`sec:lognormal`",4.783,0.09975,,,120.0,12.0
    "\ :math:`X_3`\", 	"NA", ":ref:`sec:lognormal`",4.783,0.09975,,,120.0,12.0
    "\ :math:`X_4`\", 	"NA", ":ref:`sec:lognormal`",4.783,0.09975,,,120.0,12.0
    "\ :math:`X_5`\", 	"NA", ":ref:`sec:lognormal`",3.892,0.198,,,50.0,10.0
    "\ :math:`X_6`\", 	"NA", ":ref:`sec:lognormal`",3.669,0.198,,,40.0,8.0

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_8_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_8.gfun_8

Matlab
^^^^^^^^^

See |gfun_8.m| on GitLab.

.. |gfun_8.m| raw:: html

   <a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/gfun/gfun_8.m" target="_blank">gfun_8.m</a>

.. raw:: html

   <hr>
