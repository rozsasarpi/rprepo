.. _sec:rp_111:

=========================
RP111
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 11
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",   \ :math:`7.65\cdot10^{-7}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 4.81
	"Number of performance functions", 		1
	"Reference", 							[Breitung2019]_



Performance function
-------------------------

.. math::
   :label: eq:rp_111

   		   \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = {{{5^2}} \over 2} - \left| {{X_1} \cdot {X_2}} \right|  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_2`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
 


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_111_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_111.gfun_111

.. raw:: html

   <hr>
