.. _sec:rp_213:

=====
RP213
=====

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 14

Overview
--------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Numeric (finite element analysis)
	"Number of random variables", 			13
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`2.84\cdot10^{-4}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.45
	"Number of performance functions", 		1
	"Reference", 							[Blatman2010]_


Performance function
--------------------

Too complex to be written here, see the implementation in the code repository.


Random variables
----------------

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\"
    "\ :math:`X_{13}`\", 	"NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0


The random variables are mutually independent.


Visualization
-------------

.. image:: _static/gfun_matrix_plot/rp_213_matrix.png


Implementation
--------------

Python
^^^^^^^^^

.. autofunction:: gfun_213_le_frame.gfun_213_le_frame


.. raw:: html

  <hr>
