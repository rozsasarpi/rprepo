.. _sec:rp_110:

=========================
RP110
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 10
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`3.19\cdot10^{-5}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 4.00
	"Number of performance functions", 		2
	"Reference", 							[Breitung2019]_



Performance function
-------------------------

.. math::
   :label: eq:rp_110

         \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) = \left\{ \matrix{
          4 - {X_1}{\rm{             , }}{X_1} > 3.5{\rm{ }} \hfill \cr
          0.85 - 0.1 \cdot {X_1}{\rm{  , }}{X_1} \le 3.5 \hfill \cr}  \right.  \cr
          & {g_{\mathrm{comp},2}}({\bf X}) = \left\{ \matrix{
          0.5 - 0.1 \cdot {X_2}{\rm{    , }}{X_2} > 2 \hfill \cr
          2.3 - {X_2}{\rm{          , }}{X_2} \le 2 \hfill \cr}  \right.  \cr
          & g({\bf X}) = \min \left\{ \matrix{
          {g_{\mathrm{comp},1}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},2}}({\bf X}) \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_110_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_110.gfun_110

.. raw:: html

   <hr>
