.. _sec:rp_54: 

=========================
RP54
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 7
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			20
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`9.98\cdot10^{-4}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.09
	"Number of performance functions", 		1
	"Reference", 							[Engelund1993]_



Performance function
-------------------------

.. math::
   :label: eq:rp_54

   		   \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = \sum\limits_{i = 1}^{20} {{X_i} - 8.951}   \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:exponential`",1.0,,,,1.0,1.0
    "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\"
    "\ :math:`X_{20}`\",  "NA", ":ref:`sec:exponential`",1.0,,,,1.0,1.0
 

The random variables are identically distributed and mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_54_matrix.png


Implementation
-------------------------

.. autofunction:: gfun_54.gfun_54

.. raw:: html

   <hr>
