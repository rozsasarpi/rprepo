.. _sec:rp_203: 

=========================
RP203
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 13
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Numerical (finite element analysis surrogate)
	"Number of random variables", 			4
	"Failure probability, :math:`P_\mathrm{f}`", \ :math:`4.33\cdot10^{-7}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 4.92
	"Number of performance functions", 		1
	"Reference", 							[Eijnden2019]_



Performance function
--------------------

Too complex to be written here, see the implementation in the code repository.


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\", 	"NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{2}`\", 	"NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{3}`\", 	"NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{4}`\", 	"NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_203_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_203_bram_geotech.gfun_203

.. raw:: html

   <hr>
