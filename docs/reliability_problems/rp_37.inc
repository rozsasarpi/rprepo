.. _sec:rp_37:

=========================
RP37
=========================


 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								DACC
	"Number of random variables", 			5
	"Failure probability, :math:`P_\mathrm{f}`",  DACC	
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", DACC
	"Number of performance functions", 				DACC
	"Continuity", 							DACC
	"Reference", 							DACC



Performance function
-------------------------

.. math::
   :label: eq:rp_37

   		   DACC


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\", 	"NA", ":ref:`sec:normal`",547.2,5.472
     "\ :math:`X_2`\", 	"NA", ":ref:`sec:normal`",5,0.015
     "\ :math:`X_3`\", 	"NA", ":ref:`sec:normal`",20,0.3
     "\ :math:`X_4`\", 	"NA", ":ref:`sec:normal`",5.1,0.0102
     "\ :math:`X_5`\", 	"NA", ":ref:`sec:normal`",5,0.01


The random variables are mutually independent.

Visualization
-------------------------
 
DACC


Implementation
-------------------------

DACC

.. raw:: html

   <hr>
