.. _sec:rp_25: 

=========================
RP25
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 1
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`6.14\cdot10^{-6}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 4.36
	"Number of performance functions", 		2
	"Reference", 							[Kurtz2013]_



Performance function
-------------------------

.. math::
   :label: eq:rp_25

   		   \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) = X_1^2 - 8 \cdot {X_2} + 16  \cr
          & {g_{\mathrm{comp},2}}({\bf X}) =  - 16{X_1} + {X_2} + 32  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = \max \left\{ \matrix{
          {g_{\mathrm{comp},1}}{\bf X}) \hfill \cr
          {g_{\mathrm{comp},2}}({\bf X}) \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_25_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_111.gfun_111

.. raw:: html

   <hr>
