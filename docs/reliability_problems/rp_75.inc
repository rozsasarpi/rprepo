.. _sec:rp_75:

=========================
RP75
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 9
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			2
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`1.07\cdot10^{-2}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 2.33
	"Number of performance functions", 				1
	"Reference", 							[Waarts2000]_



Performance function
-------------------------

.. math::
   :label: eq:rp_75

   		   \eqalign{
          & {g_{\mathrm{comp}}}({\bf X}) = 3 - {X_1} \cdot {X_2}  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_2`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
 


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_75_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_75.gfun_75

.. raw:: html

   <hr>
