.. _sec:rp_202:

=====
RP202
=====

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 11

Overview
--------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Numerical (finite element analysis)
	"Number of random variables", 			225
	"Failure probability, :math:`P_\mathrm{f}`", \ :math:`6.03\cdot10^{-5}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.43
	"Number of performance functions", 		2
	"Reference", 							[Schueller2007]_


Performance function
--------------------

Too complex to be written here, see the implementation in the code repository.


Random variables
----------------

.. csv-table::
  :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
  :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10
  
    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\"
    "\ :math:`X_{225}`\",  "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
  
The random variables are mutually independent.


Visualization
-------------

..
    .. image:: _static/gfun_matrix_plot/rp_202_matrix.png


Implementation
--------------

Python
^^^^^^^^^

.. autofunction:: gfun_202_shear_beam.gfun_202


.. raw:: html

  <hr>
