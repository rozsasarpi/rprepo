.. _sec:rp_63:

=========================
RP63
=========================

.. csv-table:: :ref:`tab:challenge set_1`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    1, 8
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			100
	"Failure probability, :math:`P_\mathrm{f}`",   \ :math:`3.79\cdot10^{-4}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.36
	"Number of performance functions", 		1
	"Reference", 							[Depina2016]_


Performance function
-------------------------

.. math::
   :label: eq:rp_63

          \eqalign{
           & {g_{\mathrm{comp}}}({\bf X}) = 0.1 \cdot \sum\limits_{i = 2}^{100} {X_i^2}  - {X_1} - 4.5  \cr
           & {g_{\mathrm{sys}}}({\bf X}) = {g_{\mathrm{comp}}}({\bf X}) \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\", "\ :math:`\vdots`\"
    "\ :math:`X_{100}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    
 


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_63_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_63.gfun_63

.. raw:: html

   <hr>
