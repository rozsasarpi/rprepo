.. _sec:rp_91:

=========================
RP91
=========================

.. csv-table:: :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    2, 9
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			5
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`6.97\cdot10^{-4}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 3.19
	"Number of performance functions", 		3
	"Reference", 							[Bichon2011]_



Performance function
-------------------------

.. math::
   :label: eq:rp_91

         \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) = 0.847 + 0.96 \cdot {X_2}{\rm{ + }}0.986 \cdot {X_3} - 0.216 \cdot {X_4} + {\rm{ }}0.077 \cdot X_2^2 + 0.11 \cdot X_3^2 + {{0.007 \cdot X_{_4}^2} \over {0.378}} - {\rm{ }}{X_2} \cdot {\rm{ }}{X_3} - 0.106{\rm{ }} \cdot {X_2} \cdot {X_4} - {\rm{ }}0.11 \cdot {X_3} \cdot {X_4}  \cr
          & {g_{\mathrm{comp},2}}({\bf X}) = {{8400 \cdot {X_1}} \over {\root 2 \of {X_3^2 + X_4^2 - {X_3} \cdot {X_4} + 3 \cdot X_5^2} }} - 1  \cr
          & {g_{\mathrm{comp},3}}({\bf X}) = {{8400 \cdot {X_1}} \over {\left| {{X_4}} \right|}} - 1  \cr
          & g({X}) = \min \left\{ \matrix{
          {g_{\mathrm{comp},1}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},2}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},3}}({\bf X}) \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:normal`",0.07433,0.005,,,0.07433,0.005
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:normal`",0.1,0.01,,,0.1,0.01
    "\ :math:`X_{3}`\",   "NA", ":ref:`sec:normal`",13.0,60.0,,,13.0,60.0
    "\ :math:`X_{4}`\",   "NA", ":ref:`sec:normal`",4751.0,48.0,,,4751.0,48.0
    "\ :math:`X_{5}`\",   "NA", ":ref:`sec:normal`",-684.0,11.0,,,-684.0,11.0


The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_91_matrix.png


Implementation
-------------------------

Python
^^^^^^^^^

.. autofunction:: gfun_91.gfun_91

.. raw:: html

   <hr>
