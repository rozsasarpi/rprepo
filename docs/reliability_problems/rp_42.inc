.. _sec:rp_42:

=========================
RP42
=========================


 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								DACC
	"Number of random variables", 			3
	"Failure probability, :math:`P_\mathrm{f}`",  DACC	
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", DACC
	"Number of performance functions", 				DACC
	"Continuity", 							DACC
	"Reference", 							DACC



Performance function
-------------------------

.. math::
   :label: eq:rp_42

   		   DACC


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_1`\", 	"NA", ":ref:`sec:normal`",10,0.4
     "\ :math:`X_2`\", 	"NA", ":ref:`sec:normal`",\ :math:`2\cdot10^{7}`\,\ :math:`7.5\cdot10^{6}`\ 
     "\ :math:`X_3`\", 	"NA", ":ref:`sec:normal`",\ :math:`8\cdot10^{-4}`\,\ :math:`1.5\cdot10^{-4}`\ 


The random variables are mutually independent.

Visualization
-------------------------
 
DACC


Implementation
-------------------------

DACC

.. raw:: html

   <hr>
