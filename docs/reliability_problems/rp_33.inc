.. _sec:rp_33:

=========================
RP33
=========================

.. csv-table:: :ref:`tab:tutorial_set`; :ref:`tab:challenge set_2`
   :header: "``set_id``", "``problem_id``"
   :widths: 20, 20

    -1, 3
    2, 2
 
Overview
-------------------------

.. csv-table::
   :header: "Category", "Value"
   :widths: 20, 20

    "Type", 								Symbolic
	"Number of random variables", 			3
	"Failure probability, :math:`P_\mathrm{f}`",  \ :math:`2.57\cdot10^{-3}`\
	"Reliability index, :math:`\beta=-\Phi^{-1}(P_\mathrm{f})`", 2.80
	"Number of performance functions", 		2
	"Reference", 							[Grooteman2011]_



Performance function
-------------------------

.. math::
   :label: eq:rp_33

   		   \eqalign{
          & {g_{\mathrm{comp},1}}({\bf X}) =  - {X_1} - {X_2} - {X_3} + 3 \cdot \root 2 \of 3   \cr
          & {g_{\mathrm{comp},2}}\left( {\bf X} \right) =  - {X_3} + 3  \cr
          & {g_{\mathrm{sys}}}({\bf X}) = \min \left\{ \matrix{
          {g_{\mathrm{comp},1}}({\bf X}) \hfill \cr
          {g_{\mathrm{comp},2}}\left( {\bf X} \right) \hfill \cr}  \right. \cr}


Random variables
-------------------------

The parametrization of distributions follows that of in :ref:`sec:distributions`.

.. csv-table::
   :header: "Variable", "Description", "Distribution", ":math:`\\theta_1`", ":math:`\\theta_2`", ":math:`\\theta_3`", ":math:`\\theta_4`", Mean, Std
   :widths: 10, 10, 10, 10, 10, 10, 10, 10, 10

    "\ :math:`X_{1}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{2}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0
    "\ :math:`X_{3}`\",   "NA", ":ref:`sec:normal`",0.0,1.0,,,0.0,1.0

The random variables are mutually independent.

Visualization
-------------------------
 
.. image:: _static/gfun_matrix_plot/rp_33_matrix.png


Implementation
-------------------------

.. autofunction:: gfun_33.gfun_33

.. raw:: html

   <hr>
