.. _sec:contributors_sponsors:

***********************************
Contributors & Sponsors
***********************************

.. _sec:contributors:

===================================
Contributors
===================================

* Arpad Rozsas [*]_ [1]_ [2]_ [3]_ [4]_ [6]_ [7]_ [8]_ [9]_
* Arthur Slobbe [*]_ [1]_ [4]_ [5]_ [6]_ [8]_
* Shehryar Ali [*]_ [2]_ [3]_
* Tianxiang Wang [*]_ [6]_ [7]_
* Jean-Marc Bourinet [*]_ [5]_
* Liesette la Gasse [*]_ [4]_ [6]_ [7]_
* Rein de Vries [*]_ [4]_ [6]_
* Juan Gonzalo Sepulveda Astudillo [*]_ [5]_ [6]_
* Michael Havbro Faber [*]_ [5]_
* Giulia Martini [*]_ [4]_ [6]_ [7]_ [9]_
* Bram van den Eijnden [*]_ [5]_

We are thankful to

* Eric Bonet for helping with the initial setup of the web server hosting.
* Xuzheng Chai, Giulia Martini, Nadieh Meinen, and Wim Courage for helping with testing the web server.
* Agnieszka Bigaj van Vliet and Henk Miedema for supporting the project.

.. [1] concept
.. [2] web server design
.. [3] web server implementation
.. [4] Sphinx documentation
.. [5] collecting reliability problems
.. [6] Python and/or Matlab implementation of the performance functions, LaTeX equations
.. [7] visualization and plots
.. [8] project management (securing budget, planning, task definition, etc.)
.. [9] post-processing results

.. note:: 
	If multiple persons contributed to a category, the references to each contributor appear in round brackets after
	the footnote number in square brackets.

------------

.. [*] arpad.rozsas@tno.nl
.. [*] arthur.slobbe@tno.nl
.. [*] shehry3894@gmail.com
.. [*] sysuwangtx@hotmail.com
.. [*] jean-marc.bourinet@sigma-clermont.fr
.. [*] liesette.lagasse@tno.nl
.. [*] rein.devries@tno.nl
.. [*] jgsa@civil.aau.dk
.. [*] mfn@civil.aau.dk
.. [*] giulia.martini@tno.nl
.. [*] A.P.vandenEijnden@tudelft.nl


.. _sec:sponsors:

===================================
Sponsors
===================================

The following organizations have generously contributed to the development and maintenance of the challenge framework and repository:

* |TNO|: the development work was mainly financed by TNO, the working hours of those involved from TNO and Shehryar Ali's were covered by TNO.
* |Diana|: generously provided 10 professional Diana licenses to facilitate the surrogating of computationally demanding numerical models.

.. image:: _static/tno_logo.png
   :height: 50px
   :alt: TNO

.. image:: _static/diana_logo.png
   :height: 100px
   :alt: Diana

.. |TNO| raw:: html

   <a href="https://www.tno.nl/en/" target="_blank">TNO</a>

.. |Diana| raw:: html

   <a href="https://dianafea.com/" target="_blank">Diana</a>   