.. List of References

*************************
References
*************************

.. [Grooteman2011] Grooteman, F. (2011). An adaptive directional importance sampling method for structural reliability. Probabilistic Engineering Mechanics, 26(2), 134-141. doi:http://dx.doi.org/10.1016/j.probengmech.2010.11.002.

.. [Xu2018] Xu J., Kong F. (2018). A new unequal-weighted sampling method for efficient reliability analysis. Reliability Engineering & System Safety. Volume 172. 2018. Pages 94-102.

.. [Rozsas2018] Rozsas A., Slobbe A., Meinen N. (2018). Computational Challenges in the Reliability Assessment of Engineering Structures. 2018 January 24. Delft. https://www.reliabilitytno.com/.

.. [Podila2013] Podila P. (2013). HTTP: The Protocol Every Web Developer Must Know - Part 1. 2013 April 8. https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177.

.. [Corum2003] Corum J. (2003). Estimating the Airspeed Velocity of an Unladen Swallow. 2003 November 17. http://style.org/unladenswallow/.

.. [Ditlevsen2007] Ditlevsen O., Madsen H.O. (2007). Structural Reliability Methods. June-September 2007. http://od-website.dk/books/OD-HOM-StrucRelMeth-Ed2.3.7.pdf.

.. [Schueller2004] Schuëller, G.I. & Pradlwarter, Helmut & Koutsourelakis, P. (2004). A critical appraisal of reliability estimation procedures for high dimensions. Probabilistic Engineering Mechanics. 19. Pages 463-474. doi: https://doi.org/10.1016/j.probengmech.2004.05.004.

.. [Dai2016] Dai, H., Zhang, H., & Wang, W. (2016). A new maximum entropy-based importance sampling for reliability analysis. doi: https://doi.org/10.1016/j.strusafe.2016.08.001.

.. [Sepulveda2019] Sepulved, J., Faber, M. (2019). Benchmark of Emerging Structural Reliability Methods. doi: https://doi.org/10.22725/ICASP13.439.

.. [Slobbe2019] Slobbe, A. Rozsas, A. Allaix, D. Bigaj-van Vliet, A. (2019). On the value of a reliability‐based nonlinear finite element analysis approach in the assessment of concrete structures. doi:  https://doi.org/10.1002/suco.201800344.

.. [Schueller2007]  Schuëller, G.I. Pradlwarter, H.J. (2007). Benchmark study on reliability estimation in higher dimensions of structural systems – An overview. doi: https://doi.org/10.1016/j.strusafe.2006.07.010.

.. [Blatman2010] Blatman G., Sudret B. (2010). An adaptive algorithm to build up sparse polynomial chaos expansions for stochastic finite element analysis. Probabilistic Engineering Mechanics, 25(2), Pages 183–197. doi: https://doi.org/10.1016/j.probengmech.2009.10.003.

.. [Eijnden2019] van den Eijnden A.P., Schweckendiek T., Hicks M.A. (2019). Metamodelling for geotechnical reliability analysis with noisy and incomplete models. Submitted to Structural Safety.

.. [Breitung2019] Breitung K. (2019). The geometry of limit state function graphs and
    subset simulation: Counterexamples. Reliability Engineering & System Safety. 182.
    98-106. https://doi.org/10.1016/j.ress.2018.10.008

.. [Li2018] Li X., Gong C., Gu L., Gao W., Jing Z., Su H. (2018). A sequential surrogate method for reliability analysis based on radial basis function. Structural Safety. Volume 73. 2018. Pages 42-53.

.. [Kurtz2013] Kurtz N., Song J. (2013). Cross-entropy-based adaptive importance sampling using Gaussian mixture. Structural Safety. Volume 42. 2013. Pages 35-44.

.. [Yun2018] Yun W., Lu Z., Zhang Y., Jiang X. (2018). An efficient global reliability sensitivity analysis algorithm based on classification of model output and subset simulation. Structural Safety. Volume 74. 2018. Pages 49-57.

.. [Xiao2018] Xiao N-C., Zuo M.J., Guo W. (2018). Efficient reliability analysis based on adaptive sequential sampling design and cross-validation. Applied Mathematical Modelling. Volume 58. 2018. Pages 404-420.

.. [Engelund1993] Engelund S., Rackwitz R. (1993). A benchmark study on importance sampling techniques in structural reliability. Structural Safety. Volume 12. 1993. Pages 255-276.

.. [Jiang2017] Jiang Z., Li J. (2017). High dimensional structural reliability with dimension reduction. Structural Safety. Volume 69. 2017. Pages 35-46.

.. [Wei2018] Wei P., Liu F., Tang, C. (2018). Reliability and reliability-based importance analysis of structural systems using multiple response Gaussian process model. Reliability Engineering and System Safety. Volume 175. 2018. Pages 183-195.

.. [Depina2016] Depina I., Le T.M.H., Fenton G., Eiksund G. (2016). Reliability analysis with Metamodel Line Sampling. Structural Safety. Volume 60. 2016. Pages 1-15.

.. [Waarts2000] Waarts P.H. (2000). Structural reliability using finite element analysis. PhD thesis. Delft University of Technology.

.. [Bichon2011] Bichon B.J., McFarland J.M., Mahadevan, S. (2011). Efficient surrogate models for reliability analysis of systems with multiple failure modes. Reliability Engineering and System Safety. Volume 96. 2011. Pages 1386–1395.
