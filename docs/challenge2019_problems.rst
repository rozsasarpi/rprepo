.. _sec:challenge_2019_problems:

*************************
Challenge problems
*************************

=========================
Overview
=========================

The reliability problems -- grouped into different sets -- are summarized in the tables below. The sets are established to represent distinctively different problems where different methods are expected to excel, e.g. reliability problems with a single design point, or reliability problems with continuous and differentiable performance function. Sets and problems are identified with their ``set_id`` and ``problem_id``, respectively; both are integers.

You can download all of the probabilistic models from the GitLab repository in a tabular format by clicking on |Probabilistic models|.


.. |Probabilistic models| raw:: html

	<a href="https://gitlab.com/rozsasarpi/rprepo/blob/master/code_rp/misc/probabilistic_models_up.csv" target="_blank">this link</a>
	
=========================
Problem sets
=========================

.. _sec:tutorial_set:

-------------------------
Tutorial set
-------------------------

To help the understanding and verification purposes all details are disclosed for the first two problems in this set. Problem 3 exemplifies what information will be provided during the challenge for each reliability problems. Since these problems serve testing, no upper limit on the number of performance function evaluations is enforced. Use ``username: 'testuser'`` and ``password: 'testpass'`` for testing (only available for the tutorial).

.. csv-table::
	:widths: 5, 5

	``username``, 'testuser'
	``password``, 'testpass'
.. 
	make a problem four with evaluation limit: should be only accessible with a non-default username = after registration

.. _tab:tutorial_set:

.. csv-table:: -- Tutorial set.
	:header: "``set_id``", "``problem_id``", "Reliability problem", "Max evaluations"
	:widths: 5, 5, 10, 10

	-1, 1, ":ref:`sec:rp_8`", "\ :math:`\infty`\"
	-1, 2, ":ref:`sec:rp_22`", "\ :math:`\infty`\"
	-1, 3, ":ref:`sec:rp_33`", "\ :math:`\infty`\"

-----------------------------------------------
Challenge set 1 - single performance function
-----------------------------------------------

Reliability problems in this set have a single performance function but nothing else is disclosed about them. Once the challenge is open (:ref:`sec:timeline`) you can register and use your username and password to access these problems.

Evaluating the performance function yields a component and a system performance function value. These are equal as we have only a single performance function.

.. note::
	Some of the performance functions rely on iterative numerical algorithms which might fail to converge under certain inputs; similarly as a finite element analysis. In those cases NaN values are returned. Make sure that your reliability algorithm can handle these cases. The performance functions will not yield NaN values for reasonable input points that do not correspond to failure.
	
.. _tab:challenge set_1:

.. csv-table:: -- Challenge set 1.
	:header: "``set_id``", "``problem_id``", "Reliability problem", "Max evaluations"
	:widths: 5, 5, 10, 10

	1, 1,         ":ref:`sec:rp_14`",                             10000
	1, 2,         ":ref:`sec:rp_24`",                             10000
	1, 3,         ":ref:`sec:rp_28`",                             10000
	1, 4,         ":ref:`sec:rp_31`",                             10000
	1, 5,         ":ref:`sec:rp_38`",                             10000
	1, 6,         ":ref:`sec:rp_53`",                             10000
	1, 7,         ":ref:`sec:rp_54`",                             10000
	1, 8,         ":ref:`sec:rp_63`",                             10000
	1, 9,         ":ref:`sec:rp_75`",                             10000
	1, 10,        ":ref:`sec:rp_107`",                            10000
	1, 11,        ":ref:`sec:rp_111`",                            10000
	1, 12,        ":ref:`sec:rp_201`",                            10000
	1, 13,        ":ref:`sec:rp_203`",                            10000
	1, 14,        ":ref:`sec:rp_213`",                            10000
	1, 15,        ":ref:`sec:rp_300`",                            10000
	1, 16,        ":ref:`sec:rp_301`",                            10000



-------------------------------------------------------------------------
Challenge set 2 - Multiple, known number of performance functions
-------------------------------------------------------------------------

Reliability problems in this set have unknown number of performance functions. Once the challenge is open (:ref:`sec:timeline`) you can register and use your username and password to access these problems.

Evaluating the performance function yields performance function values for each component and the system as well. However, how the system performance function is calculated is not revealed, you test the :ref:`sec:rp_33` system problem from the tutorial set.

.. note::
	Some of the performance functions rely on iterative numerical algorithms which might fail to converge under certain inputs; similarly as a finite element analysis. In those cases NaN values are returned. Make sure that your reliability algorithm can handle these cases.

.. _tab:challenge set_2:

.. csv-table:: -- Challenge set 2.
	:header: "``set_id``", "``problem_id``", "Reliability problem", "Max evaluations"
	:widths: 5, 5, 10, 10

	2, 1,         ":ref:`sec:rp_25`",                             10000
	2, 2,         ":ref:`sec:rp_33`",                             10000
	2, 3,         ":ref:`sec:rp_35`",                             10000
	2, 4,         ":ref:`sec:rp_55`",                             10000
	2, 5,         ":ref:`sec:rp_57`",                             10000
	2, 6,         ":ref:`sec:rp_60`",                             10000
	2, 7,         ":ref:`sec:rp_77`",                             10000
	2, 8,         ":ref:`sec:rp_89`",                             10000
	2, 9,         ":ref:`sec:rp_91`",                             10000
	2, 10,        ":ref:`sec:rp_110`",                            10000
	2, 11,        ":ref:`sec:rp_202`",                            10000
