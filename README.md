# RPrepo

[![Documentation Status](https://readthedocs.org/projects/rprepo/badge/?version=latest)](https://rprepo.readthedocs.io/en/latest/?badge=latest)

**RPrepo: A reliability problems repository.**

Problem repository of the Black-box Reliability Challenge 2019.

The online documentation is automatically rebuilt after pushing a change to the master or development branches of the repository:
https://rprepo.readthedocs.io/en/latest/index.html

The repository is meant to be an open and collaboratively developed compendium of reliability problems, hence all contributions are welcome. If you are not familiar or uncomfortable with [git](https://git-scm.com/) you can send comments and contributions directly to us: <arpad.rozsas@tno.nl>. If you would like to use git but you are having troubles with it we would gladly assist you.

## Requirements
 * the code is developed under python 3.7.2 (and Windows operating system)
 * the `requirements.txt` file contains all the packages which are needed for a local build.

## For contributors

The main principles of organizing the code:
 * Make the modules functions as standardized, systematic, and consistent as possible.
 * Automate wherever it is possible: reduce double entry and possible errors from manual entry.
 * Follow the best practices in Sphinx documentation, e.g. [Altair](https://github.com/altair-viz/altair).

### reStructuredText (rst) files

* Files with `.rst`, `.inc`, and `.plot` are all rst files. They have different attributes to indicate different conceptual classes.

### Performance functions

 * The performance function (gfun) implementations are in and should be put into the `code_rp\gfun` folder.
 * Each performance function has its own file.
 * The applied naming convention: `gfun_<number>.py` and/or `gfun_<number>.m`.
 * All functions should have the same structure: input, output, input checking, error catching, and docstring documentation. See the existing functions and use them as templates. 	
 * Use the [numpy docstring](https://numpydoc.readthedocs.io/en/latest/format.html) documentation rules for creating a documentation for each function, these will be used to automatically generate the online documentation.
 * TODO: the reliability problem pages should be automatically generated from templates, e.g. using [jinja2](http://jinja.pocoo.org/docs/) as for the distribution plots.

### Distribution plotting functions

 * The distribution implementations are in and should be put into the `code_rp\distributions` folder.
 * The plotting is done by using the [Altair](https://github.com/altair-viz/altair) package.
 * Each plot has its own file.
 * The naming convention: `altair_<name>_<pdf/cdf>.py`.
 * The rst files with plots are automatically generated with name: `<name>_<pdf/cdf>.plot`.
 * Include these in the dedicated rst files of distributions, 
   i.e. in `docs/distributions/<name>_dist.inc` should be a line: `.. include:: distributions/<name>_<pdf/cdf>.plot`.
