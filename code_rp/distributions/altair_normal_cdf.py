"""
Plot normal cumulative probability distribution functions using altair
"""

import altair as alt
import pandas as pd
import numpy as np

from scipy.stats import norm

# keep the same range and moments for each distribution
# -------------------------------------------------------
x    = np.linspace(-4, 6, 200)
mean = np.array([0.5, 1.0, 1.0, 2.0])
var  = np.array([0.5, 1.0, 5.0, 1.0])
# -------------------------------------------------------

loc = mean
scale = np.sqrt(var)

n = len(loc)

df = pd.DataFrame(columns=['x', 'F', 'dist'])

for ii in range(n):
  F = norm.cdf(x,loc=loc[ii],scale=scale[ii])
  df_ii = pd.DataFrame({'x' : x,
                    'F' : F
                    })
  df_ii['dist'] =  'mean=' + str(round(mean[ii],3)) + ', variance=' + str(round(var[ii],3)) +\
                   ' (\u03bc=' + str(round(loc[ii],3)) + ', \u03c3\u00b2=' + str(round(scale[ii]**2,3)) + ')'
  df = df.append(df_ii)

highlight = alt.selection(type='single', on='mouseover',
                      fields=['dist'], nearest=True)

base = alt.Chart(df).encode(
    x='x:Q',
    y='F:Q',
    color=alt.Color('dist:N', legend=alt.Legend(title="Distributions", orient="top-left"))
)

base.encode(alt.Y(scale=alt.Scale(domain=[0, 1])))

points = base.mark_circle().encode(
    opacity=alt.value(0)
).add_selection(
    highlight
).properties(
    width=500
)

lines = base.mark_line().encode(
  size=alt.condition(~highlight, alt.value(1), alt.value(3))
)

alt.layer(points, lines).configure_legend(labelLimit=0)