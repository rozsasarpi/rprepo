"""
Plot Uniform cumulative probability distribution functions using Altair
"""

import altair as alt
import pandas as pd
import numpy as np

from scipy.stats import uniform

# keep the same range and moments for each distribution
# -------------------------------------------------------
x    = np.linspace(-4, 6, 10)
mean = np.array([0.5, 1.0, 1.0, 2.0])
var  = np.array([0.5, 1.0, 5.0, 1.0])
# -------------------------------------------------------

a    = mean - np.sqrt(3*var)
b    = mean + np.sqrt(3*var)

x    = np.append(x, [a, b])
x.sort()

n = len(a)

df = pd.DataFrame(columns=['x', 'F', 'dist'])

for ii in range(n):
  F = uniform.cdf(x, loc=a[ii], scale=b[ii]-a[ii])
  df_ii = pd.DataFrame({'x' : x,
                    'F' : F
                    })
  df_ii['dist'] =  'mean=' + str(round(mean[ii],3)) + ', variance=' + str(round(var[ii],3)) + \
                   ' (a=' + str(round(a[ii],3)) + ', b=' + str(round(b[ii],3)) + ')'
  df = df.append(df_ii)

highlight = alt.selection(type='single', on='mouseover',
                      fields=['dist'], nearest=True)

base = alt.Chart(df).encode(
    x='x:Q',
    y='F:Q',
    color=alt.Color('dist:N', legend=alt.Legend(title="Distributions", orient="top-left"))
)

points = base.mark_circle().encode(
    opacity=alt.value(0)
).add_selection(
    highlight
).properties(
    width=500
)

lines = base.mark_line().encode(
  size=alt.condition(~highlight, alt.value(1), alt.value(3))
)

alt.layer(points, lines).configure_legend(labelLimit=0)