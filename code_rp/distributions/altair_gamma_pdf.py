"""
Plot Gamma probability density function using Altair
"""

import altair as alt
import pandas as pd
import numpy as np

from scipy.stats import gamma

# keep the same range and moments for each distribution
# -------------------------------------------------------
x    = -1 + np.logspace(0, np.log(7)/ np.log(10), 100)
x    = np.insert(x, 0, [-4,-0.001])
mean = np.array([0.5, 1.0, 1.0, 2.0])
var  = np.array([0.5, 1.0, 5.0, 1.0])
# -------------------------------------------------------

k    = var/mean
scale= mean**2/var

n = len(loc)

df = pd.DataFrame(columns=['x', 'f', 'dist'])

for ii in range(n):
  f = gamma.pdf(x, a=k[ii], scale=scale[ii])
  df_ii = pd.DataFrame({'x' : x,
                    'f' : f
                    })
  df_ii['dist'] =  'mean=' + str(round(mean[ii],3)) + ', variance=' + str(round(var[ii],3)) + \
                   ' (k=' + str(round(k[ii],3)) + ', \u03b8=' + str(round(scale[ii],3)) + ')'
  df = df.append(df_ii)

highlight = alt.selection(type='single', on='mouseover',
                      fields=['dist'], nearest=True)

base = alt.Chart(df).encode(
    x='x:Q',
    y='f:Q',
    color=alt.Color('dist:N', legend=alt.Legend(title="Distributions", orient="top-left"))
)

points = base.mark_circle().encode(
    opacity=alt.value(0)
).add_selection(
    highlight
).properties(
    width=500
)

lines = base.mark_line().encode(
  size=alt.condition(~highlight, alt.value(1), alt.value(3))
)

alt.layer(points, lines).configure_legend(labelLimit=0)