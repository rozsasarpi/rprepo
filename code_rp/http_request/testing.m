clearvars
close all
clc

username    = "testuser";
password    = "testpass";
set_id      = -1;
problem_id  = 2;
x           = [0.545, 1.23];
% x           = [1.23];

[g_val_sys, g_val_comp, msg]    = evaluate(username, password, set_id, problem_id, x);

disp(msg)
disp(g_val_sys)

g_sys_fun   = @(y) evaluate(username, password, set_id, problem_id, y);

n           = 10;
xx          = linspace(1,10,n);
gg          = nan(n,1);

tic
for ii = 1:n
   gg(ii) = g_sys_fun([xx(ii), x(2)]);
end
toc

plot(xx, gg)
