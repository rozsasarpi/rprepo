# -*- coding: utf-8 -*-

from submit import submit
 
username    = 'testuser'
password    = 'testpass'
set_id      = -1
problem_id  = 2
beta_sys    = 3.4
beta_comp   = 3.4
alpha_sys   = []
alpha_comp  = [0.64, 0.77]

msg = submit(username, password, set_id, problem_id, beta_sys, beta_comp, alpha_sys, alpha_comp)

print(msg)

