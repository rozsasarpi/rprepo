% Submit temporarily and final results of a reliability analysis
%
%SYNOPSYS
% out = SUBMIT(username, password, set_id, problem_id, beta_sys, beta_comp, alpha_sys, alpha_comp)
%
%INPUT
% username: string
%   Registered username for authentication. For testing without registration use 'testuser'.
% password : string
%   Registered password for authentication. For testing without registration use 'testpass'.
% set_id : integer
%   Identification number of the problem set.
% problem_id : integer
%   Identification number of the problem.
% beta_sys : scalar
%   System reliability index.
% beta_comp : vector
%   Vector of reliability indices. Leave it empty [] if you do not want to
%   submit it.
% alpha_sys : vector
%   Vector of sensitivity factors. Leave it empty [] if you do not want to
%   submit it.
% alpha_comp : matrix
%   Matrix of sensitivity factors. Each row is a vector component sensitivity factors.
%   Leave it empty [] if you do not want to submit it.
%   
%OUTPUT
% msg : string
%   Diagnostic message.
%

function msg = submit(username, password, set_id, problem_id, beta_sys, beta_comp, alpha_sys, alpha_comp)

% -----------------------------------------------
% Pre-processing
% -----------------------------------------------

if nargin < 8
   alpha_comp = [];
end
if nargin < 7
   alpha_sys = [];
end
if nargin < 6
   beta_comp = [];
end

beta_comp       = preprocess_input(beta_comp);
alpha_sys       = preprocess_input(alpha_sys);
alpha_comp      = preprocess_input(alpha_comp);

inp.username    = username;
inp.password    = password;
inp.set_ID      = set_id;
inp.problem_ID  = problem_id;
inp.beta_sys    = cell_wrap(beta_sys);
inp.beta_comp   = cell_wrap(beta_comp);
inp.alpha_sys   = cell_wrap(alpha_sys);
inp.alpha_comp  = cell_wrap(alpha_comp);

main_url        = 'https://tno-black-box-challenge-api.herokuapp.com/';
options         = weboptions('MediaType','application/json');

% -----------------------------------------------
% HTTP request
% -----------------------------------------------

msg             = webwrite([main_url, 'submit'], inp, options);

% -----------------------------------------------
% Nested functions
% -----------------------------------------------

    function y = cell_wrap(x)
        [m, n] = size(x);
        if (m == 1) || (n == 1)
            x = x(:)';
        end
        [m, ~] = size(x);
        
        if m == 1
            y = num2cell(x);
        else
            y = num2cell(x, 2);
        end
    end

    function x = preprocess_input(x)
        if all(all(isnan(x))) || isempty(x)
            x = [];
        end
    end
end