% Evaluate a performance function
%
%SYNOPSYS
% [g_val_sys, g_val_comp, msg] = EVALUATE(username, password, set_id, problem_id, x)
%
%INPUT
% username: string
%   Registered username for authentication. For testing without registration use 'testuser'.
% password : string
%   Registered password for authentication. For testing without registration use 'testpass'.
% set_id : integer
%   Identification number of the problem set.
% problem_id : integer
%   Identification number of the problem.
% x : matrix
%   Values of independent variables/random variables where the performance function is evaluated. Columns are the values of random variables (x1, x2,...xn). Bundle (vectorized) call is possible by providing multiple rows, each corresponds to one set of values of the random variables.
%
%
%OUTPUT
% g_val_sys : matrix
%   Performance function value on system level.
% g_val_comp : matrix
%   Performance function value fo each component.
% msg : string
%   Diagnostic message.
%
%NOTES
% currently only one request a time, i.e. no vectorized input

function [g_val_sys, g_val_comp, msg] = evaluate(username, password, set_id, problem_id, x)

% -----------------------------------------------
% Pre-processing
% -----------------------------------------------

% collect input in a struct
inp.username    = username;
inp.password    = password;
inp.set_ID      = set_id;
inp.problem_ID  = problem_id;
inp.input_list  = cell_wrap(x);

main_url        = 'https://tno-black-box-challenge-api.herokuapp.com/';
options         = weboptions('MediaType','application/json');
options.Timeout = 30;

% -----------------------------------------------
% HTTP request
% -----------------------------------------------

out_json        = webwrite([main_url, 'evaluate'], inp, options);

% -----------------------------------------------
% Post-processing
% -----------------------------------------------

if exist('jsondecode', 'builtin') == 5
    out             = jsondecode(out_json);
    g_val_sys       = out.g_val_sys;
    g_val_comp      = out.g_val_comp.';
    msg             = out.msg;
else
    C               = parse_json(out_json);
    S               = C{1};
    g_val_sys       = cell_unwrap(S.g_val_sys).';
    g_val_comp      = cell_unwrap(S.g_val_comp);
    msg             = S.msg;
end

% -----------------------------------------------
% Nested functions
% -----------------------------------------------

    function y = cell_wrap(x)
        [m, n] = size(x);
        if (m == 1) || (n == 1)
            x = x(:)';
        end
        [m, ~] = size(x);
        
        if m == 1
            y = num2cell(x);
        else
            y = num2cell(x, 2);
        end
    end

    function x = cell_unwrap(y)
        if iscell(y{1})
            nx = length(y);
            mx = length(y{1});
            x = nan(nx, mx);
            for ii = 1:nx
                x(ii,:) = cell2mat(y{ii});
            end
        else
            x = cell2mat(y).';
        end
        x = x.';
    end

end

