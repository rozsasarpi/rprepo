# -*- coding: utf-8 -*-

from evaluate import evaluate
 
username    = 'testuser'
password    = 'testpass'
set_id      = -1
problem_id  = 2
x           = [0.545, 1.23]

g_val_sys, g_val_comp, msg = evaluate(username, password, set_id, problem_id, x)

print(msg)
print(g_val_sys)
print(g_val_comp)
