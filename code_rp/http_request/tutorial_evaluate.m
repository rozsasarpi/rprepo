clearvars
close all
clc

username    = 'testuser';
password    = 'testpass';
set_id      = -1;
problem_id  = 2;
x           = [0.545, 1.23];

[g_val_sys, g_val_comp, msg] = evaluate(username, password, set_id, problem_id, x);

disp(msg)
disp(g_val_sys)
disp(g_val_comp)

g_sys_fun = @(y) evaluate(username, password, set_id, problem_id, y);

g_sys_fun(x)
