

def submit(username, password, set_id, problem_id, beta_sys, beta_comp=None, alpha_sys=None, alpha_comp=None):
    """Evaluate a performance function

    Parameters
    ----------
        username : str
            Registered username for authentication. For testing without registration use 'testuser'.
        password : str
            Registered password for authentication. For testing without registration use 'testpass'.
        set_id : int
            Identification number of the problem set.
        problem_id : int
            Identification number of the problem.
        beta_sys : float
            System reliability index.
        beta_comp : list of float
            Vector of reliability indices. Leave it empty [] if you do not want to submit it.
        alpha_sys : list of float
            Vector of sensitivity factors. Leave it empty [] if you do not want to submit it.
        alpha_comp : list of (list of) float
            Matrix of sensitivity factors. Each row is a vector component sensitivity factors. Leave it empty [] if you do not want to submit it.

    Returns
    -------
        msg : str
            Accompanying diagnostic message, e.g. warning.

    Examples
    --------
        >>> msg = submit(username='testuser', password='testpass', set_id=-1, problem_id=3, beta_sys=3.4, beta_comp=np.array([3.4, 4.5]), alpha_sys=None, alpha_comp=np.array([[0.64, 0.77], [0.84, 0.54]]))
        >>> print(msg)
    """
    # -----------------------------------------------
    # Pre-processing
    # -----------------------------------------------
    import requests
    import numpy as np

    def preprocess_input(x):
        if isinstance(x, (float, int)):
             return [float(x)]
        elif isinstance(x, np.ndarray):
            return x.tolist()
        elif x is None:
            return []
        else:
            return x

    beta_sys = preprocess_input(beta_sys)
    beta_comp = preprocess_input(beta_comp)
    alpha_sys = preprocess_input(alpha_sys)
    alpha_comp = preprocess_input(alpha_comp)

    main_url = 'https://tno-black-box-challenge-api.herokuapp.com/'

    body = {'username': username,
            'password': password,
            'set_ID': set_id,
            'problem_ID': problem_id,
            'beta_sys': beta_sys,
            'beta_comp': beta_comp,
            'alpha_sys': alpha_sys,
            'alpha_comp': alpha_comp,
            }

    # -----------------------------------------------
    # HTTP request
    # -----------------------------------------------
    r = requests.post(main_url + 'submit', json=body)

    # -----------------------------------------------
    # Post-processing
    # -----------------------------------------------
    msg = r.text

    return msg
