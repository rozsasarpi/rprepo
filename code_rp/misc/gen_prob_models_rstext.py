"""
Generates restructured text table body for the Sphinx documentation
Run gen_prob_models_up before
"""

import pandas as pd
import numpy as np
import os

cwd = os.getcwd()

df = pd.read_csv(os.path.join(cwd, 'probabilistic_models_up.csv'))


# custom rounding to significant digits (engineering)
def round_to_n(x, n):
    if np.isnan(x) or not isinstance(x, (float, int, np.ndarray)):
        y = ''
    elif x == 0:
        y = x
    else:
        if x/(10**n) > 1:
            y = round(x)
        else:
            y = round(x, -int(np.floor(np.log10(abs(x)))) + (n - 1))
    return y


n_df = len(df)
n_round = 4

rp_id = df['reliability_problem_id'].unique()
n_rp = len(rp_id)

with open('probabilistic_models_rstext.md', 'w') as open_file:
    for ii in range(n_rp):
        open_file.write(f'\n\nRP{rp_id[ii]}\n========\n')

        idx = df['reliability_problem_id'] == rp_id[ii]
        df_ii = df[idx].reset_index()
        n_df_ii = len(df_ii)

        for jj in range(n_df_ii):
            dist = df_ii['distribution_type'][jj]
            par = df_ii.loc[jj, 'theta_1':'theta_4'].tolist()
            mom = df_ii.loc[jj, ['mean', 'std']].tolist()

            par1 = round_to_n(par[0], n_round)
            par2 = round_to_n(par[1], n_round)
            par3 = round_to_n(par[2], n_round)
            par4 = round_to_n(par[3], n_round)

            mean = round_to_n(mom[0], n_round)
            std = round_to_n(mom[1], n_round)

            open_file.write(f'    "\\ :math:`X_{{{jj+1}}}`\\", 	"NA", ":ref:`sec:{dist}`",{par1},{par2},{par3},{par4},{mean},{std}\n')
