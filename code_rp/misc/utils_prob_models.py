"""
Pre-process the probabilistic input for the reliability problems

With eval it could be generalized and made more consice..
"""

import numpy as np


def mom2par(mom, dist):
    mean = mom[0]
    var = mom[1]

    if dist == 'uniform':
        par = uniform_mom2par(mean, var)
    elif dist == 'lognormal':
        par = lognormal_mom2par(mean, var)
    elif dist == 'gumbel_max':
        par = gumbel_max_mom2par(mean, var)
    elif dist == 'exponential':
        par = exponential_mom2par(mean, var)
    elif dist == 'normal':
        par = [mean, np.sqrt(var), np.nan, np.nan]
    else:
        par = np.nan

    par4 = np.ones(4)
    par4[:] = np.nan
    n = len(par)
    par4[:n] = par

    return par4


def par2mom(par, dist):
    par1 = par[0]
    par2 = par[1]

    if dist == 'uniform':
        mom = uniform_par2mom(par1, par2)
    elif dist == 'lognormal':
        mom = lognormal_par2mom(par1, par2)
    elif dist == 'gumbel_max':
        mom = gumbel_max_par2mom(par1, par2)
    elif dist == 'exponential':
        mom = exponential_par2mom(par1)
    elif dist == 'normal':
        mom = [par1, par2**2]
    else:
        raise ValueError(f'Unknown distribution type: {dist}')

    mom2 = np.ones(2)
    mom2[:] = np.nan
    n = len(mom)
    mom2[:n] = mom

    return mom


def uniform_mom2par(mean, var):
    """
    """
    a = mean - np.sqrt(3 * var)
    b = mean + np.sqrt(3 * var)
    return [a, b]


def uniform_par2mom(a, b):
    """
    """
    mean = (a + b)/2
    var = (b - mean)**2/3
    return [mean, var]


def lognormal_mom2par(mean, var):
    """
    shape: \mu
    scale: \sigma
    """
    shape = np.log(mean ** 2 / np.sqrt(var + mean ** 2))
    scale = np.sqrt(np.log(var / mean ** 2 + 1))
    return [shape, scale]


def lognormal_par2mom(shape, scale):
    """
    shape: \mu
    scale: \sigma
    """
    mean = np.exp(shape + scale**2/2)
    cov = np.sqrt(np.exp(scale**2) - 1)
    var = (mean*cov)**2
    return [mean, var]


def gumbel_max_mom2par(mean, var):
    """
    loc: \mu
    scale: \beta
    """
    gamma = np.euler_gamma
    scale = np.sqrt(6)/np.pi*np.sqrt(var)
    loc = mean - scale*gamma
    return [loc, scale]


def gumbel_max_par2mom(loc, scale):
    """
    loc: \mu
    scale: \beta
    """
    gamma = np.euler_gamma
    mean = loc + scale*gamma
    var = np.pi**2/6*scale**2
    return [mean, var]


def exponential_mom2par(mean):
    """
    rate: \lambda
    """
    rate = 1/mean
    return [rate]


def exponential_par2mom(rate):
    """
    rate: \lambda
    """
    mean = 1/rate
    var = mean**2
    return [mean, var]
