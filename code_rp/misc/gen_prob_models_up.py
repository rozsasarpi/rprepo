"""
Updates the probabilistic models: parameters and moments are both provided
"""

import pandas as pd
import numpy as np
import os

#from code.misc.utils_prob_models import mom2par, par2mom
from utils_prob_models import mom2par, par2mom

cwd = os.getcwd()

#df = pd.read_csv(os.path.join(cwd, 'code', 'misc', 'probabilistic_models.csv'))
df = pd.read_csv(os.path.join(cwd, 'probabilistic_models.csv'))

n_df = len(df)

# mom provided: both mean and std are provided
# mom not provided: neither mean or std are provided
#
diff_lim = 1e-3

df_up = df
for ii in range(n_df):
    dist = df['distribution_type'][ii]
    par = df.loc[ii, 'theta_1':'theta_4'].tolist()
    mom = df.loc[ii, ['mean', 'std']].tolist()
    # var -> std
    mom[1] = mom[1]**2

    par_up = par
    mom_up = mom

    # only mom provided
    if all(~np.isnan(mom)) and all(np.isnan(par)):
        par_up = mom2par(mom, dist)
    # only par provided
    elif all(np.isnan(mom)) and any(np.isnan(par)):
        mom_up = par2mom(par, dist)
    # both mom and par provided
    elif all(~np.isnan(mom)) and any(np.isnan(par)):
        par_calc = mom2par(mom, dist)
        n_par = len(par_calc)
        for jj in range(n_par):
            diff = abs(par_calc[jj] - par[jj])/par[jj]
            if diff > diff_lim:
                raise ValueError(f'The provided parameters and moments do not match! For parameter_{jj+1} ' +
                                 f'the difference in calculated and provided is {diff} > diff_lim={diff_lim}. ' +
                                 f'Row={ii+1} (not counting the header).')
    # neither mom or par provided
    elif all(np.isnan(mom)) and all(np.isnan(par)):
        print(f'Warning! In row={ii+1} (not counting the header) no sufficient par nor mom are provided. The row is kept as is.')

    else:
        print(f'Warning! In row={ii+1} a case is encountered where none of the input checking conditions are met! The row is kept as is. We recommend to examine the code.')

    # var -> std
    mom_up[1] = np.sqrt(mom_up[1])
    df_up.loc[ii, 'theta_1':'theta_4'] = par_up
    df_up.loc[ii, ['mean', 'std']] = mom_up

# Save the full table
df_up.to_csv(os.path.join(cwd, 'probabilistic_models_up.csv'), index=False)
