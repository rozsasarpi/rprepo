"""
Automatically generates rst files from templates.

gen_distr_plot_rst_files()   
    reStructuredText files with probability density (pdf) and cumulative distribution functions (cdf) are generated.
    A template is used for the generation.

# Inspired by altair, altairgallery.py, utils.py
"""

import glob
import re

from jinja2 import Template
from utils import (get_docstring_and_rest)


distr_plot_template = Template(u"""
.. This reStructuredText document is auto-generated. Do not modify it directly.

.. altair-plot::
    :hide-code:

    {{ code | indent(4) }}

.. toctree::
   :hidden:
""")
    
# NOTE: everything should be set relative to the location of conf.py

def gen_distr_plot_rst_files():
    """Generate rst files for pdf and cdf plots

    The plots are made in altair and in separate py files.

    Parameters
    ----------

    Returns
    -------
    """

    # get all the plotting files, distributions
    dist_plots = [re.findall("altair_(.*?).py", file)[0] for file in glob.glob("../code_rp/distributions/altair*.py")]
    # generate rst files for each distribution
    for name in dist_plots:
        source_filename = '../code_rp/distributions/altair_' + name + '.py'
        target_filename = 'distributions/' + name + '.plot'
        
        docstring, rest, lineno = get_docstring_and_rest(source_filename)
            
        with open(target_filename, 'w', encoding='utf-8') as f:
            f.write(distr_plot_template.render(code=rest))
            print(target_filename + ' has been generated.')


def gen_rp_rst_files():
    # TODO
    return

# required for sphinx to accept that your module as an extension module
def setup(app):
    return