def gfun_202(x):
    """Performance function for reliability problem 202.

    Parameters
    ----------
        x : numpy.array of float(s)
            Values of independent variables: columns are the different parameters/random variables (x1, x2,...xn) and rows are different parameter/random variables sets for different calls.

    Returns
    -------
        g_val_sys : numpy.array of float(s)
            Performance function value for the system.
        g_val_comp : numpy.array of float(s)
            Performance function value for each component.
        msg : str
            Accompanying diagnostic message, e.g. warning.
    """
    import numpy as np
    from code_rp.gfun.shear_beam.shear_beam import shear_beam

    # expected number of random variables/columns
    nrv_e = 225

    g, g1, g2 = float('nan'), float('nan'), float('nan')
    msg = 'Ok'
    x = np.array(x, dtype='f')

    n_dim = len(x.shape)
    if n_dim == 1:
        x = np.array(x)[np.newaxis]
    elif n_dim > 2:
        msg = 'Only available for 1D and 2D arrays.'
        return float('nan'), float('nan'), msg

    nrv_p = x.shape[1]
    if nrv_p != nrv_e:
        msg = f'The number of random variables (x, columns) is expected to be {nrv_e} but {nrv_p} is provided!'
    else:
        n_call = x.shape[0]
        g1 = np.zeros((n_call, 1))
        g2 = np.zeros((n_call, 1))
        for i in range(n_call):
            drift = shear_beam(x[i, :], std_normal=True)
            g1[i] = 0.032 - drift[0]
            g2[i] = 0.022 - drift[4]
        g = np.amin(np.stack((g1, g2)), 0)

    g_val_sys = g
    g_val_comp = np.stack((g1, g2))
    return g_val_sys, g_val_comp, msg
