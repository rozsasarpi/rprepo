def bram_geotech(x):

    import numpy as np
    import pickle

    with open('code_rp/gfun/bram_geotech/metamodel.pkl', 'rb') as openfile:
        predictor = pickle.load(openfile)

    with open('code_rp/gfun/bram_geotech/errormodel.pkl', 'rb') as openfile:
        classifier = pickle.load(openfile)

    # support area - outside NaN
    I = classifier.predict(x)
    I = I.astype(bool)

    G = predictor.predict(x)
    G[I] = np.nan

    return G, I