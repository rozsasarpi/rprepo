
import numpy as np
from math import pi, sin, cos, exp, sqrt, floor
from scipy.stats import norm
# from numba import jit

####################################################################################################
# Generic functions

def sq(x):
    return x * x


def interpolate(y, dx, x, x0=0.0):
    i = int(floor((x - x0) / dx))
    j = i + 1

    if i < 0:
        return y[0]

    if j >= len(y):
        return y[-1]

    x_i = x0 + i * dx
    x_j = x0 + j * dx

    y_i = y[i]
    y_j = y[j]

    return (x_j * y_i - x_i * y_j + (y_j - y_i) * x) / dx


####################################################################################################
# Components of ground motion signal

# For 1-based indexing:
#   k = 0  ->  2 * k + 1 = 1  ->  cos
#	       ->  2 * k + 2 = 2  ->  sin
#   k = 1  ->  2 * k + 1 = 3  ->  cos
#	       ->  2 * k + 2 = 4  ->  sin
#   k = 2  ->  2 * k + 1 = 5  ->  cos
#	       ->  2 * k + 2 = 6  ->  sin
#   etc.
#
# For 0-based indexing:
#   k = 0  ->  2 * k	 = 0  ->  cos
#	       ->  2 * k + 1 = 1  ->  sin
#   k = 1  ->  2 * k	 = 2  ->  cos
#	       ->  2 * k + 1 = 3  ->  sin
#   k = 2  ->  2 * k	 = 4  ->  cos
#	       ->  2 * k + 1 = 5  ->  sin
#   etc.

def gm_components():
    global components

    duration = 20.0
    dt = 0.01
    n = int(round(duration / dt) + 1)

    A = 0.1
    B = 0.1

    m = 200

    # components = [None] * n

    #
    # tic()
    t1 = np.arange(n).reshape((n, 1)) * dt

    fade1 = np.exp(-0.1 * (t1 - 10.0))
    fade1[t1 < 10.0] = 1.0
    fade1[t1 < 2.0] = t1[t1 < 2.0] / 2.0

    k1 = np.arange(0,m)
    k2 = np.arange(0,m)
    k1[k2%2==0] = k1[k2%2==0] / 2
    k1[k2%2==1] = (k1[k2%2==1] - 1) / 2

    omega1 = np.ones((n,1)) * (2.0 * pi * (1.0 + 0.1 * k1))

    components = np.zeros(shape=(n, m))
    temp1 = fade1 * A * np.cos(omega1 * t1)
    temp2 = fade1 * B * np.sin(omega1 * t1)
    components[:, ::2] = temp1[:, ::2]
    components[:, 1::2] = temp2[:, 1::2]

    # toc()
    #

    # tic()
    # for i in range(0, n):
    #     t = i * dt
    #
    #     if t < 2.0:
    #         fade = t / 2.0
    #     elif t < 10.0:
    #         fade = 1.0
    #     else:
    #         fade = exp(-0.1 * (t - 10.0))
    #
    #     components[i] = [None] * 200
    #
    #     for j in range(0, 200, 2):
    #         k = j / 2
    #         omega = 2.0 * pi * (1.0 + 0.1 * k)
    #         components[i][j] = fade * A * cos(omega * t)
    #
    #     for j in range(1, 200, 2):
    #         k = (j - 1) / 2
    #         omega = 2.0 * pi * (1.0 + 0.1 * k)
    #         components[i][j] = fade * B * sin(omega * t)
    #
    # toc()
    return components


####################################################################################################
# Create ground motion signal
#@jit(nopython=True)
def gm_signal(comps, psi):
    duration = 20.0
    dt = 0.01
    n = int(round(duration / dt)) + 1

    a = np.zeros(n)
    a = a + (psi*comps).sum(axis=1)

    return a, dt


####################################################################################################
# Bilinear hysteretic behaviour

def F_bilinear(k_e, d_y, alpha, d, d_prev, F_prev):
    if d == d_prev:
        return F_prev

    # calculate elastic force

    F = F_prev + k_e * (d - d_prev)

    # make sure it does not exceed backbone curve

    F_y = k_e * d_y
    k_s = alpha * k_e

    F = min(F_y + k_s * (d - d_y), F)
    F = max(-F_y - k_s * (-d - d_y), F)

    return F


####################################################################################################
# FEM response calculation

#   d_gm
#    =>
#
#    /|     k_0     -----     k_1     -----     k_2     -----
#    /|----/\/\/---| m_1 |---/\/\/---| m_2 |---/\/\/---| m_3 |--- Etc.
#    /|             -----             -----             -----
#
#    =>               =>                =>                =>
#   d_0              d_1               d_2               d_3

def calc_response(m, c, k_e, d_y, alpha, gm_a, gm_dt, dt):
    # check parameters

    if len(k_e) < 1 or len(k_e) != len(m) - 1 or len(gm_a) < 1:
        return []

    # initialise values and vectors needed in the calculation

    duration = (len(gm_a) - 1) * gm_dt
    timesteps = int(round(duration / dt)) + 1

    nodes = len(m)
    elements = len(k_e)

    drift = [0.0] * elements  # maximum absolute displacement between floors

    d_n = [0.0] * nodes  # node displacements at t_n
    d_np1 = [0.0] * nodes  # node displacements at t_{n+1}
    d_nm1 = [0.0] * nodes  # node displacements at t_{n-1}
    v_n = [0.0] * nodes  # node velocities at t_n
    F_s_n = [0.0] * nodes  # element spring force
    F_d_n = [0.0] * nodes  # element damper force
    d_prev = [0.0] * nodes  # hysteretic model previous diplacement
    F_prev = [0.0] * nodes  # hysteretic model previous force

    input_a_nm1 = 0.0
    input_v_nm1 = 0.0
    input_d_nm1 = 0.0

    # perform explicit time integration

    for i_timestep in range(0, timesteps):
        t_n = i_timestep * dt

        # integrate ground motion acceleration to velocity and displacement

        input_a_n = interpolate(gm_a, gm_dt, t_n)

        input_v_n = input_v_nm1 + (input_a_nm1 + input_a_n) / 2.0 * dt
        input_d_n = input_d_nm1 + (input_v_nm1 + input_v_n) / 2.0 * dt

        # move displacements forward

        for i in range(0, nodes):
            d_nm1[i] = d_n[i]
            d_n[i] = d_np1[i]

        # apply precribed motion

        d_n[0] = input_d_n

        # calculate velocities at time t_n (used for damper forces)

        for i in range(0, nodes):
            v_n[i] = (d_n[i] - d_nm1[i]) / dt

        # calculate internal element forces

        for i in range(0, elements):
            d_rel = d_n[i + 1] - d_n[i]
            v_rel = v_n[i + 1] - v_n[i]

            F_s_n[i] = F_bilinear(k_e[i], d_y[i], alpha[i], d_rel, d_prev[i], F_prev[i])
            d_prev[i] = d_rel
            F_prev[i] = F_s_n[i]

            F_d_n[i] = c[i] * v_rel

            drift[i] = max(abs(d_rel), drift[i])

        # calculate displacements in next timestep for each node
        # no need to calculate the left node, because it has a prescribed motion

        # middle nodes

        for i in range(1, nodes - 1):
            F_e_n = 0.0  # external force acting on node
            F_s_n_node = F_s_n[i - 1] - F_s_n[i]  # left spring force acting on node
            F_d_n_node = F_d_n[i - 1] - F_d_n[i]  # left damper force acting on node

            d_np1[i] = sq(dt) / m[i] * (F_e_n - F_s_n_node - F_d_n_node - \
                                        1.0 / sq(dt) * m[i] * d_nm1[i] + 2.0 / sq(dt) * m[i] * d_n[i])

        # right node

        i = nodes - 1

        F_e_n = 0.0  # external force acting on node
        F_s_n_node = F_s_n[i - 1] - 0.0  # left spring force acting on node
        F_d_n_node = F_d_n[i - 1] - 0.0  # left damper force acting on node

        d_np1[i] = sq(dt) / m[i] * (F_e_n - F_s_n_node - F_d_n_node - \
                                    1.0 / sq(dt) * m[i] * d_nm1[i] + 2.0 / sq(dt) * m[i] * d_n[i])

        # store ground motion acceleration and velocity for next timestep

        input_a_nm1 = input_a_n
        input_v_nm1 = input_v_n
        input_d_nm1 = input_d_n

    return drift


####################################################################################################
# Calculate drifts for each storey

components = []


def shear_beam(x, std_normal=False):
    global components

    if len(x) != 225:
        return []

    # create ground motion signal
    psi = [x[25 + i] for i in range(0, 200)]

    if len(components) == 0:
        components = gm_components()

    gm_a, gm_dt = gm_signal(np.array(components), np.array(psi))

    # mean and standard deviation
    mu_m = [20e3] * 5
    sigma_m = [0.1 * z for z in mu_m]

    mu_k = [24e6, 21e6, 18e6, 15e6, 12e6]
    sigma_k = [0.1 * z for z in mu_k]

    mu_r = [0.1 * z for z in mu_k]
    sigma_r = [0.1 * z for z in mu_r]

    mu_d_y = [8e-3] * 5
    sigma_d_y = [0.1 * z for z in mu_d_y]

    mu_xi = [0.06] * 5
    sigma_xi = [0.01] * 5

    # apply transformation if necessary

    if std_normal:
        # censor input through inverse transform sampling

        F_min = norm.cdf(-5.0)
        F_max = norm.cdf(5.0)

        x = [norm.ppf(F_min + norm.cdf(x[i]) * (F_max - F_min)) for i in range(0, 25)]

        # realisations

        m = [mu_m[i] + sigma_m[i] * x[i] for i in range(0, 5)]
        k = [mu_k[i] + sigma_k[i] * x[i + 5] for i in range(0, 5)]
        r = [mu_r[i] + sigma_r[i] * x[i + 10] for i in range(0, 5)]
        d_y = [mu_d_y[i] + sigma_d_y[i] * x[i + 15] for i in range(0, 5)]
        xi = [mu_xi[i] + sigma_xi[i] * x[i + 20] for i in range(0, 5)]

    else:
        # assign directly

        m = [x[i] for i in range(0, 5)]
        k = [x[i + 5] for i in range(0, 5)]
        r = [x[i + 10] for i in range(0, 5)]
        d_y = [x[i + 15] for i in range(0, 5)]
        xi = [x[i + 20] for i in range(0, 5)]

        # check bounds

        for i in range(0, 5):
            if m[i] < mu_m[i] - 5.0 * sigma_m[i] or m[i] > mu_m[i] + 5.0 * sigma_m[i]:
                return []
            if k[i] < mu_k[i] - 5.0 * sigma_k[i] or k[i] > mu_k[i] + 5.0 * sigma_k[i]:
                return []
            if r[i] < mu_r[i] - 5.0 * sigma_r[i] or r[i] > mu_r[i] + 5.0 * sigma_r[i]:
                return []
            if d_y[i] < mu_d_y[i] - 5.0 * sigma_d_y[i] or d_y[i] > mu_d_y[i] + 5.0 * sigma_d_y[i]:
                return []
            if xi[i] < mu_xi[i] - 5.0 * sigma_xi[i] or xi[i] > mu_xi[i] + 5.0 * sigma_xi[i]:
                return []

    # hysteretic model parameters

    k_e = [k[i] + r[i] for i in range(0, 5)]
    alpha = [r[i] / k_e[i] for i in range(0, 5)]
    c = [2.0 * xi[i] * sqrt(m[i] * k_e[i]) for i in range(0, 5)]

    # calculate the response, decrease timestep if not converged
    m.insert(0, 0.0)

    for i in range(0, 3):
        calc_dt = 5e-3 / pow(10.0, i)

        drift = calc_response(m, c, k_e, d_y, alpha, gm_a, gm_dt, calc_dt)

        if max(drift) < 10.0:
            break  # calculation converged (i.e. not exploded)
    return drift
