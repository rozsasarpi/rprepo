% Performance function for reliability problem 22.
% 
% [g_val_sys, g_val_comp, msg] = GFUN_22(x)
%
%
%     Parameters
%     ----------
%         x : matrix
%             Values of independent variables: columns are the different parameters/random variables (x1, x2,...xn) and rows are different parameter/random variables sets for different calls.
% 
%     Returns
%     -------
%         g_val_sys : matrix
%             Performance function value for the system.
%         g_val_comp : matrix
%             Performance function value for each component.
%         msg : str
%             Accompanying diagnostic message, e.g. warning.

function [g_val_sys, g_val_comp, msg] = gfun_22(x)

% -------------------------------------------------------------------------
% INITIALIZE
% -------------------------------------------------------------------------
% expected number of random variables/columns
nrv_e = 2;
% if any(size(x) == 1)
%     x = x(:).';
% end

msg = 'Ok';
g = NaN;

% -------------------------------------------------------------------------
% EVALUATE
% -------------------------------------------------------------------------
nrv_p = size(x,2);
n_dim = length(size(x));

if nrv_p ~= nrv_e
    msg = ['The number of random variables (x, columns) is expected to be ', num2str(nrv_e), ' but ', num2str(nrv_p), ' is provided!'];
elseif n_dim > 2
    msg = 'Only available for 1D and 2D matrices.';
else
    g = 2.5 - 1/sqrt(2)*(x(:,1) + x(:,2)) + 0.1*(x(:,1) - x(:,2)).^2;
end

g_val_sys = g;
g_val_comp = g;

end