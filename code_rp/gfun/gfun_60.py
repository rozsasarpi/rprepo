def gfun_60(x):
    """Performance function for reliability problem 60.

    Parameters
    ----------
        x : numpy.array of float(s)
            Values of independent variables: columns are the different parameters/random variables (x1, x2,...xn) and rows are different parameter/random variables sets for different calls.

    Returns
    -------
        g_val_sys : numpy.array of float(s)
            Performance function value for the system.
        g_val_comp : numpy.array of float(s)
            Performance function value for each component.
        msg : str
            Accompanying diagnostic message, e.g. warning.
    """
    import numpy as np
    # expected number of random variables/columns
    nrv_e = 5

    g, g1, g2, g3 = float('nan'), float('nan'), float('nan'), float('nan')
    g4, g5, g6, g7 = float('nan'), float('nan'), float('nan'), float('nan')
    msg = 'Ok'
    x = np.array(x, dtype='f')

    n_dim = len(x.shape)
    if n_dim == 1:
        x = np.array(x)[np.newaxis]
    elif n_dim > 2:
        msg = 'Only available for 1D and 2D arrays.'
        return float('nan'), float('nan'), msg

    nrv_p = x.shape[1]
    if nrv_p != nrv_e:
        msg = f'The number of random variables (x, columns) is expected to be {nrv_e} but {nrv_p} is provided!'
    else:
        g1 = x[:, 0] - x[:, 4]
        g2 = x[:, 1] - x[:, 4] / 2
        g3 = x[:, 2] - x[:, 4] / 2
        g4 = x[:, 3] - x[:, 4] / 2
        g5 = x[:, 1] - x[:, 4]
        g6 = x[:, 2] - x[:, 4]
        g7 = x[:, 3] - x[:, 4]
        gmin234 = np.amin(np.stack((g2, g3, g4)), 0)
        gmin56 = np.amin(np.stack((g5, g6)), 0)
        gmax1 = np.amax(np.stack((gmin56, g7)), 0)
        gmax2 = np.amax(np.stack((gmin234, gmax1)), 0)
        g = np.amin(np.stack((g1, gmax2)), 0)

    g_val_sys = g
    g_val_comp = np.stack((g2, g3, g4, g5, g6, g7))
    return g_val_sys, g_val_comp, msg
