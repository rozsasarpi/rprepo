def gfun_110(x):
    """Performance function for reliability problem 25.

    Parameters
    ----------
        x : numpy.array of float(s)
            Values of independent variables: columns are the different parameters/random variables (x1, x2,...xn) and rows are different parameter/random variables sets for different calls.

    Returns
    -------
        g_val_sys : numpy.array of float(s)
            Performance function value for the system.
        g_val_comp : numpy.array of float(s)
            Performance function value for each component.
        msg : str
            Accompanying diagnostic message, e.g. warning.
    """
    import numpy as np
    # expected number of random variables/columns
    nrv_e = 2

    g, g1, g2 = float('nan'), float('nan'), float('nan')
    msg = 'Ok'
    x = np.array(x, dtype='f')

    n_dim = len(x.shape)
    if n_dim == 1:
        x = np.array(x)[np.newaxis]
    elif n_dim > 2:
        msg = 'Only available for 1D and 2D arrays.'
        return float('nan'), float('nan'), msg

    nrv_p = x.shape[1]
    if nrv_p != nrv_e:
        msg = f'The number of random variables (x, columns) is expected to be {nrv_e} but {nrv_p} is provided!'
    else:
        g1 = np.zeros((x.shape[0], 1))
        g2 = np.zeros((x.shape[0], 1))
        for i in range(x.shape[0]):
            if x[i, 0] <= 3.5:
                g1[i] = 0.85 - 0.1 * x[i, 0]
            else:
                g1[i] = 4 - x[i, 0]
            if x[i, 1] <= 2:
                g2[i] = 2.3 - x[i, 1]
            else:
                g2[i] = 0.5 - 0.1 * x[i, 1]

        g = np.amin(np.stack((g1, g2)), 0)

    g_val_sys = g
    g_val_comp = np.stack((g1, g2))
    return g_val_sys, g_val_comp, msg
