def gfun_91(x):
    """Performance function for reliability problem 91.

    Parameters
    ----------
        x : numpy.array of float(s)
            Values of independent variables: columns are the different parameters/random variables (x1, x2,...xn) and rows are different parameter/random variables sets for different calls.

    Returns
    -------
        g_val_sys : numpy.array of float(s)
            Performance function value for the system.
        g_val_comp : numpy.array of float(s)
            Performance function value for each component.
        msg : str
            Accompanying diagnostic message, e.g. warning.
    """
    import numpy as np
    # expected number of random variables/columns
    nrv_e = 5

    g, g1, g2, g3 = float('nan'), float('nan'), float('nan'), float('nan')
    msg = 'Ok'
    x = np.array(x, dtype='f')

    n_dim = len(x.shape)
    if n_dim == 1:
        x = np.array(x)[np.newaxis]
    elif n_dim > 2:
        msg = 'Only available for 1D and 2D arrays.'
        return float('nan'), float('nan'), msg

    nrv_p = x.shape[1]
    if nrv_p != nrv_e:
        msg = f'The number of random variables (x, columns) is expected to be {nrv_e} but {nrv_p} is provided!'
    else:
        x1 = 4 * (x[:, 0] - 0.075)
        x2 = 20 * (x[:, 1] - 0.1)
        x3 = -6000 * (1 / x[:, 4] + 0.003)
        g1 = 0.847 + 0.96 * x1 + 0.986 * x2 - 0.216 * x3 + 0.077 * x1 ** 2 + 0.11 * x2 ** 2 + 0.007 * x3 ** 2 \
             + 0.378 - x1 * x2 - 0.106 * x1 * x3 - 0.11 * x2 * x3
        g2 = 84000 * x[:, 0] / np.sqrt(x[:, 2] ** 2 + x[:, 3] ** 2 - x[:, 2] * x[:, 3] + 3 * x[:, 4] ** 2) - 1
        g3 = 84000 * x[:, 0] / np.abs(x[:, 3]) - 1
        g = np.amin(np.stack((g1, g2, g3)), 0)

    g_val_sys = g
    g_val_comp = np.stack((g1, g2, g3))
    return g_val_sys, g_val_comp, msg
