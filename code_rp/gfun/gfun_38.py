def gfun_38(x):
    """Performance function for reliability problem 38.

    Parameters
    ----------
        x : numpy.array of float(s)
            Values of independent variables: columns are the different parameters/random variables (x1, x2,...xn) and rows are different parameter/random variables sets for different calls.

    Returns
    -------
        g_val_sys : numpy.array of float(s)
            Performance function value for the system.
        g_val_comp : numpy.array of float(s)
            Performance function value for each component.
        msg : str
            Accompanying diagnostic message, e.g. warning.
    """
    import numpy as np
    # expected number of random variables/columns
    nrv_e = 7

    g = float('nan')
    msg = 'Ok'
    x = np.array(x, dtype='f')

    n_dim = len(x.shape)
    if n_dim == 1:
        x = np.array(x)[np.newaxis]
    elif n_dim > 2:
        msg = 'Only available for 1D and 2D arrays.'
        return float('nan'), float('nan'), msg

    nrv_p = x.shape[1]
    if nrv_p != nrv_e:
        msg = f'The number of random variables (x, columns) is expected to be {nrv_e} but {nrv_p} is provided!'
    else:
        g = 15.59*1e4 - x[:, 0]*x[:, 1]**3/(2*x[:, 2]**3) * ((x[:, 3]**2-4*x[:, 4]*x[:, 5]*x[:, 6]**2+x[:, 3]*(x[:, 5]+4*x[:, 4]+2*x[:, 5]*x[:, 6]))/(x[:, 3]*x[:, 4]*(x[:, 3]+x[:, 5]+2*x[:, 5]*x[:, 6])))

    g_val_sys = g
    g_val_comp = g
    return g_val_sys, g_val_comp, msg
