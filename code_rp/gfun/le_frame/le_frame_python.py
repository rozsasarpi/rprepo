# le_frame.py
import numpy as np
import numpy.matlib


# def plot_results(NODE, BEAM, ne, U, nn, node):
#     import matplotlib
#     import matplotlib.pyplot as plt
#     def_scale = 20
#
#     # display all elements directly
#     # title('Deformed plot of 3D beam problem')
#     xs = np.reshape(NODE[BEAM[:, 4:6]-1, 1].T, (2, ne))
#     ys = np.reshape(NODE[BEAM[:, 4:6]-1, 2].T, (2, ne))
#
#     uu = np.reshape(U, (3, nn), order='F')
#     uu = uu[0:2, :].T
#     ux = np.reshape(uu[BEAM[:, 4:6]-1, 0].T, (2, ne))
#     uy = np.reshape(uu[BEAM[:, 4:6]-1, 1].T, (2, ne))
#
#     fig, ax = plt.subplots()
#     ax.plot(xs, ys, 'ks:')  # undeformed elements
#     ax.plot(xs + def_scale * ux, ys + def_scale * uy, 'rs-') # plot deformed elements
#     ax.axis('equal')
#     for ii in range(node.shape[0]):
#         ax.text(node[ii, 1], node[ii, 2], node[ii, 0])
#     plt.show()

# def le_frame(x, u=None):
def le_frame(x):
    cg = np.array([0.07, 0.3048])

    # node: [node_nb  x  y]
    node = np.array([
        [1,  0.0,  0.0],
        [2,  25.0, 0.0],
        [3,  55.0, 0.0],
        [4,  80.0, 0.0],
        [11, 0.0,  16.0],
        [12, 25.0, 16.0],
        [13, 55.0, 16.0],
        [14, 80.0, 16.0],
        [21, 0.0,  28.0],
        [22, 25.0, 28.0],
        [23, 55.0, 28.0],
        [24, 80.0, 28.0],
        [31, 0.0,  40.0],
        [32, 25.0, 40.0],
        [33, 55.0, 40.0],
        [34, 80.0, 40.0],
        [41, 0.0,  52.0],
        [42, 25.0, 52.0],
        [43, 55.0, 52.0],
        [44, 80.0, 52.0],
        [51, 0.0,  64.0],
        [52, 25.0, 64.0],
        [53, 55.0, 64.0],
        [54, 80.0, 64.0]
    ])

    kl = cg[1]
    node[:, 1:3] = node[:, 1:3]*kl

    # beam: [beam_nb  nb_of_elements  geo_index  mat_index  1st_node_nb  2nd_node_nb]
    beam = np.array([
        [1,  1,  7,  2,   1,  11],
        [2,  1,  8,  2,   2,  12],
        [3,  1,  8,  2,   3,  13],
        [4,  1,  7,  2,   4,  14],
        [11,  1,  6,  2,  11,  21],
        [12,  1,  7,  2, 12,  22],
        [13,  1,  7,  2,  13,  23],
        [14,  1,  6,  2,  14,  24],
        [21,  1,  6,  2,  21,  31],
        [22,  1,  7,  2,  22,  32],
        [23,  1,  7,  2,  23,  33],
        [24,  1,  6,  2,  24,  34],
        [31,  1,  5,  2,  31,  41],
        [32,  1,  6,  2,  32,  42],
        [33,  1,  6,  2,  33,  43],
        [34,  1,  5,  2,  34,  44],
        [41,  1,  5,  2,  41,  51],
        [42,  1,  6,  2,  42,  52],
        [43,  1,  6,  2,  43,  53],
        [44,  1,  5,  2,  44,  54],
        [15,  1,  3,  1,  11,  12],
        [16,  1,  4,  1,  12,  13],
        [17,  1,  3,  1,  13,  14],
        [25,  1,  2,  1,  21,  22],
        [26,  1,  3,  1,  22,  23],
        [27,  1,  2,  1,  23,  24],
        [35,  1,  2,  1,  31,  32],
        [36,  1,  3,  1,  32,  33],
        [37,  1,  2,  1,  33,  34],
        [45,  1,  1,  1,  41,  42],
        [46,  1,  2,  1,  42,  43],
        [47,  1,  1,  1,  43,  44],
        [55,  1,  1,  1,  51,  52],
        [56,  1,  2,  1,  52,  53],
        [57,  1,  1,  1,  53,  54]
    ])

    # geo: [geo_index  A  Iz](general)
    geo = np.array([
        [1,  x[17],  x[9]],  # B1 - E4
        [2,  x[18],  x[10]],  # B2 - E4
        [3,  x[19],  x[11]],  # B3 - E4
        [4,  x[20],  x[12]],  # B3 - E4
        [5,  x[13],   x[5]],  # C1 - E5
        [6,  x[14],   x[6]],  # C2 - E5
        [7,  x[15],   x[7]],  # C3 - E5
        [8,  x[16],   x[8]]  # C4 - E5
    ])

    # mats: [mat_index  E]
    mat = np.array([
        [1, x[3]],  # E4
        [2, x[4]],  # E5
    ])

    # applied point loads   :  [  node_nb  FX  FY  MZ  ]
    f = np.array([
         [11,   x[2],   0,   0],  # P3
         [21,   x[1],   0,   0],  # P2
         [31,   x[0],   0,   0],  # P1
         [41,   x[0],   0,   0],  # P1
         [51,   x[0],   0,   0]   # P1
    ])

    # zero displacement boundary conditions   :  [  node_nb  uX  uY  thetaZ  ]
    # 1: zero displacement/rotation,  0: no constraint
    ufix = np.array([
        [1,   1,   1,   1],
        [2,   1,   1,   1],
        [3,   1,   1,   1],
        [4,   1,   1,   1]
    ])

    dim = 2  # dimension of the problem
    ndofpernd = 3  # number of dof per node

    # number of beams
    nb = beam.shape[0]
    # number of elements
    ne = sum(beam[:, 1])
    # number of nodes
    nn = len(np.unique(beam[:, 4:6])) + sum(beam[:, 1] - 1)
    # number of dof
    ndof = ndofpernd*nn

    # preprocessing
    BEAM   = np.zeros((ne, beam.shape[1])).astype(int)
    numnodeini = np.unique(beam[:, 4:6])
    NODE   = np.zeros((nn, 3))
    Inode  = np.zeros((node.shape[0], 1)).astype(int)
    ne = 0
    nn = 0

    for ib in range(nb):
        neib = beam[ib, 1]
        n1ib = beam[ib, 4]
        n2ib = beam[ib, 5]

        BEAM[ne:(ne + neib), :] = np.matlib.repmat(beam[ib, :], beam[ib, 1], 1)

        ifirst = n1ib == numnodeini
        if any(ifirst):
            NODE[nn, 0] = nn + 1
            NODE[nn, 1:3] = node[n1ib == node[:, 0], 1:3]
            numnodeini[ifirst] = -1
            BEAM[ne, 4] = nn + 1
            Inode[n1ib == node[:, 0]] = nn + 1
            nn = nn + 1
        else:
            BEAM[ne, 4] = Inode[n1ib == node[:, 0]]

        if beam[ib, 1] > 1.0:
            NODE[nn:(nn + neib - 1), 0] = np.arange((nn + 1), (nn+neib-1)).T
            node1 = node[n1ib == node[:, 0], :]
            node2 = node[n2ib == node[:, 0], :]
            nodetmp = np.hstack([
                        np.linspace(node1[1], node2[1], neib + 1),
                        np.linspace(node1[2], node2[2], neib + 1),
                        np.linspace(node1[3], node2[3], neib + 1)]).T
            NODE[nn:(nn + neib - 1), 1:3] = nodetmp[1:neib, :]
            BEAM[(ne + 1):(ne + neib), 4] = np.arange((nn + 1), (nn+neib-1)).T
            BEAM[ne:(ne + neib - 1), 5] = np.arange((nn + 1), (nn+neib-1)).T
            nn = nn + (neib - 1)

        ilast = n2ib == numnodeini
        if any(ilast):
            NODE[nn, 0] = nn + 1
            NODE[nn, 1:3] = node[n2ib == node[:, 0], 1:3]
            numnodeini[ilast] = -1
            BEAM[ne + neib - 1, 5] = nn + 1
            Inode[n2ib == node[:, 0]] = nn + 1
            nn = nn + 1
        else:
            BEAM[ne + neib - 1, 5] = Inode[n2ib == node[:, 0]]

        ne = ne + neib

    BEAM[:, 1] = 1

    # BEAM, NODE, Inode, ne, nn, return

    # assembly of the stiffness matrix
    K = np.zeros((ndof, ndof))
    for ie in range(ne):

        conn = BEAM[ie, :].astype(int)

        # compute element stiffness matrix
        x1 = NODE[conn[4]-1, 1]
        x2 = NODE[conn[5]-1, 1]
        y1 = NODE[conn[4]-1, 2]
        y2 = NODE[conn[5]-1, 2]

        CC = np.array([[x2 - x1],
                       [y2 - y1]])

        L = np.linalg.norm(CC)

        c = (x2 - x1) / L
        s = (y2 - y1) / L
        R = np.array([[c, s, 0], [-s, c, 0], [0, 0, 1]])

        E = mat[BEAM[ie, 3]-1, 1]
        A = geo[BEAM[ie, 2]-1, 1]
        Iz = geo[BEAM[ie, 2]-1, 2]

        kexy = np.zeros((2 * ndofpernd, 2 * ndofpernd))
        kexy[np.ix_([0, ndofpernd], [0, ndofpernd])] = E*A/L * np.array([[1, -1], [-1, 1]])

        kk = E/L * np.array([[12*Iz/L**2,   6*Iz/L,  -12*Iz/L**2,   6*Iz/L],
                             [6*Iz/L,       4*Iz,    -6*Iz/L,       2*Iz],
                             [-12*Iz/L**2,  -6*Iz/L, 12*Iz/L**2,    -6*Iz/L],
                             [6*Iz/L,       2*Iz,    -6*Iz/L,       4*Iz]])

        kexy[np.ix_([1, 2, ndofpernd + 1, ndofpernd + 2], [1, 2, ndofpernd + 1, ndofpernd + 2])] = kk

        aux_mx = np.vstack((np.hstack((R, np.zeros((ndofpernd, ndofpernd)))), np.hstack((np.zeros((ndofpernd, ndofpernd)), R))))
        keXY = aux_mx.T @ kexy @ aux_mx

        # scatter keXY within K
        sctr = np.array([ndofpernd * conn[4] - 2,
                         ndofpernd * conn[4] - 1,
                         ndofpernd * conn[4],
                         ndofpernd * conn[5] - 2,
                         ndofpernd * conn[5] - 1,
                         ndofpernd * conn[5]])

        # scatter vector
        K[np.ix_(sctr-1, sctr-1)] = K[np.ix_(sctr-1, sctr-1)] + keXY

    # applied point load vector
    F = np.zeros((ndof, 1))
    for iF in range(f.shape[0]):
        nodeF = int(Inode[f[iF, 0] == node[:, 0]])
        F[((nodeF - 1) * ndofpernd): (nodeF * ndofpernd), 0] = f[iF, 1:4]

    # zero displacement boundary conditions; to improve!
    ifix = np.zeros((ndof, 1))
    for iu in range(ufix.shape[0]):
        nodeufix = int(Inode[ufix[iu, 0]-1])
        ifix[((nodeufix - 1) * ndofpernd):(nodeufix * ndofpernd), 0] = ufix[iu, 1:4]

    ifix = np.where(ifix != 0)[0]

    K[ifix, :] = 0  # zero out rows
    K[:, ifix] = 0  # zero out cols
    K[np.ix_(ifix, ifix)] = np.eye(len(ifix))  # put ones on the diagonal
    F[ifix] = 0

    # solve
    U = np.linalg.solve(K, F)

    g = cg[0] - U[20*3]
    # plot_results(NODE, BEAM, ne, U, nn, node)
    # plot_results(NODE, BEAM, ne, u, nn, node)
    return U[20*3]

# x = np.array([1.3345e+05,
#    8.8964e+04,
#    7.1172e+04,
#    2.1738e+10,
#    2.3796e+10,
#    8.1131e-03,
#    1.1479e-02,
#    2.1319e-02,
#    2.5893e-02,
#    1.0789e-02,
#    1.4068e-02,
#    2.3217e-02,
#    2.5893e-02,
#    3.1215e-01,
#    3.7161e-01,
#    5.0539e-01,
#    5.5742e-01,
#    2.5270e-01,
#    2.9079e-01,
#    3.7254e-01,
#    4.1806e-01])
#
# g = le_frame(x)
#
# print(g)