# le_frame.py
import numpy as np
from numba import jit


@jit(nopython=True, cache=True)
def init_frame():
    """Linear elastic analysis of a frame: calculations which are independent of x

    """
    cg = np.array([0.07, 0.3048])

    # node: [node_nb  x  y]
    node = np.array([
        [1, 0.0, 0.0],
        [2, 25.0, 0.0],
        [3, 55.0, 0.0],
        [4, 80.0, 0.0],
        [11, 0.0, 16.0],
        [12, 25.0, 16.0],
        [13, 55.0, 16.0],
        [14, 80.0, 16.0],
        [21, 0.0, 28.0],
        [22, 25.0, 28.0],
        [23, 55.0, 28.0],
        [24, 80.0, 28.0],
        [31, 0.0, 40.0],
        [32, 25.0, 40.0],
        [33, 55.0, 40.0],
        [34, 80.0, 40.0],
        [41, 0.0, 52.0],
        [42, 25.0, 52.0],
        [43, 55.0, 52.0],
        [44, 80.0, 52.0],
        [51, 0.0, 64.0],
        [52, 25.0, 64.0],
        [53, 55.0, 64.0],
        [54, 80.0, 64.0]
    ])

    kl = cg[1]
    node[:, 1:3] = node[:, 1:3] * kl

    # beam: [beam_nb  nb_of_elements  geo_index  mat_index  1st_node_nb  2nd_node_nb]
    beam = np.array([
        [1, 1, 7, 2, 1, 11],
        [2, 1, 8, 2, 2, 12],
        [3, 1, 8, 2, 3, 13],
        [4, 1, 7, 2, 4, 14],
        [11, 1, 6, 2, 11, 21],
        [12, 1, 7, 2, 12, 22],
        [13, 1, 7, 2, 13, 23],
        [14, 1, 6, 2, 14, 24],
        [21, 1, 6, 2, 21, 31],
        [22, 1, 7, 2, 22, 32],
        [23, 1, 7, 2, 23, 33],
        [24, 1, 6, 2, 24, 34],
        [31, 1, 5, 2, 31, 41],
        [32, 1, 6, 2, 32, 42],
        [33, 1, 6, 2, 33, 43],
        [34, 1, 5, 2, 34, 44],
        [41, 1, 5, 2, 41, 51],
        [42, 1, 6, 2, 42, 52],
        [43, 1, 6, 2, 43, 53],
        [44, 1, 5, 2, 44, 54],
        [15, 1, 3, 1, 11, 12],
        [16, 1, 4, 1, 12, 13],
        [17, 1, 3, 1, 13, 14],
        [25, 1, 2, 1, 21, 22],
        [26, 1, 3, 1, 22, 23],
        [27, 1, 2, 1, 23, 24],
        [35, 1, 2, 1, 31, 32],
        [36, 1, 3, 1, 32, 33],
        [37, 1, 2, 1, 33, 34],
        [45, 1, 1, 1, 41, 42],
        [46, 1, 2, 1, 42, 43],
        [47, 1, 1, 1, 43, 44],
        [55, 1, 1, 1, 51, 52],
        [56, 1, 2, 1, 52, 53],
        [57, 1, 1, 1, 53, 54]
    ])

    # zero displacement boundary conditions   :  [  node_nb  uX  uY  thetaZ  ]
    # 1: zero displacement/rotation,  0: no constraint
    ufix = np.array([
        [1,   1,   1,   1],
        [2,   1,   1,   1],
        [3,   1,   1,   1],
        [4,   1,   1,   1]
    ])

    dim = 2  # dimension of the problem
    ndofpernd = 3  # number of dof per node

    # number of beams
    nb = beam.shape[0]
    # number of elements
    ne = beam[:, 1].sum()
    # number of nodes
    nn = len(np.unique(beam[:, 4:6])) + (beam[:, 1] - 1).sum()
    # number of dof
    ndof = ndofpernd*nn

    # preprocessing
    BEAM = np.zeros((ne, beam.shape[1]), dtype=np.intp)
    numnodeini = np.unique(beam[:, 4:6])
    NODE = np.zeros((nn, 3))
    Inode = np.zeros((node.shape[0], 1), dtype=np.intp)
    ne = 0
    nn = 0

    for ib in range(nb):
        neib = beam[ib, 1]
        n1ib = beam[ib, 4]
        n2ib = beam[ib, 5]

        # BEAM[ne:(ne + neib), :] = np.matlib.repmat(beam[ib, :], beam[ib, 1], 1)
        BEAM[ne:(ne + neib), :] = beam[ib, :]

        ifirst = n1ib == numnodeini
        if ifirst.any():
            NODE[nn, 0] = nn + 1
            NODE[nn, 1:3] = node[n1ib == node[:, 0], 1:3]
            numnodeini[ifirst] = -1
            BEAM[ne, 4] = nn + 1
            Inode[n1ib == node[:, 0]] = nn + 1
            nn = nn + 1
        else:
            mask = n1ib == node[:, 0]
            BEAM[ne, 4] = Inode[mask].item()

        ilast = n2ib == numnodeini
        if ilast.any():
            NODE[nn, 0] = nn + 1
            NODE[nn, 1:3] = node[n2ib == node[:, 0], 1:3]
            numnodeini[ilast] = -1
            BEAM[ne + neib - 1, 5] = nn + 1
            Inode[n2ib == node[:, 0]] = nn + 1
            nn = nn + 1
        else:
            mask = n2ib == node[:, 0]
            BEAM[ne + neib - 1, 5] = Inode[mask].item()

        ne = ne + neib

    BEAM[:, 1] = 1

    return BEAM, NODE, Inode, node, ufix, ne, ndof


@jit(nopython=True, cache=True)
def solve_frame(x, BEAM, NODE, Inode, node, ufix, ne, ndof):
    """Linear elastic analysis of a frame: calculations which are dependent on x

    """
    dim = 2  # dimension of the problem
    ndofpernd = 3  # number of dof per node

    # geo: [geo_index  A  Iz](general)
    geo = np.array([
        [1, x[17], x[9]],  # B1 - E4
        [2, x[18], x[10]],  # B2 - E4
        [3, x[19], x[11]],  # B3 - E4
        [4, x[20], x[12]],  # B3 - E4
        [5, x[13], x[5]],  # C1 - E5
        [6, x[14], x[6]],  # C2 - E5
        [7, x[15], x[7]],  # C3 - E5
        [8, x[16], x[8]]  # C4 - E5
    ])

    # mats: [mat_index  E]
    mat = np.array([
        [1, x[3]],  # E4
        [2, x[4]],  # E5
    ])

    # applied point loads   :  [  node_nb  FX  FY  MZ  ]
    f = np.array([
        [11, x[2], 0, 0],  # P3
        [21, x[1], 0, 0],  # P2
        [31, x[0], 0, 0],  # P1
        [41, x[0], 0, 0],  # P1
        [51, x[0], 0, 0]  # P1
    ])

    # assembly of the stiffness matrix
    K = np.zeros((ndof, ndof))
    for ie in range(ne):
        conn = BEAM[ie, :]

        # compute element stiffness matrix
        x1 = NODE[conn[4] - 1, 1]
        x2 = NODE[conn[5] - 1, 1]
        y1 = NODE[conn[4] - 1, 2]
        y2 = NODE[conn[5] - 1, 2]

        CC = np.array([[x2 - x1],
                       [y2 - y1]])

        L = np.linalg.norm(CC)

        c = (x2 - x1) / L
        s = (y2 - y1) / L
        R = np.array([[c, s, 0], [-s, c, 0], [0, 0, 1.]])

        E = mat[BEAM[ie, 3] - 1, 1]
        A = geo[BEAM[ie, 2] - 1, 1]
        Iz = geo[BEAM[ie, 2] - 1, 2]

        kexy = E / L * np.array([[1 * A, 0, 0, -1 * A, 0, 0],
                                 [0, 12 * Iz / L ** 2, 6 * Iz / L, 0, -12 * Iz / L ** 2, 6 * Iz / L],
                                 [0, 6 * Iz / L, 4 * Iz, 0, -6 * Iz / L, 2 * Iz],
                                 [-1 * A, 0, 0, 1 * A, 0, 0],
                                 [0, -12 * Iz / L ** 2, -6 * Iz / L, 0, 12 * Iz / L ** 2, -6 * Iz / L],
                                 [0, 6 * Iz / L, 2 * Iz, 0, -6 * Iz / L, 4 * Iz]
                                 ])

        aux_mx = np.vstack(
            (np.hstack((R, np.zeros((ndofpernd, ndofpernd)))), np.hstack((np.zeros((ndofpernd, ndofpernd)), R))))
        keXY = aux_mx.T @ kexy @ aux_mx

        # scatter keXY within K
        pos1 = ndofpernd * conn[4] - 3
        pos2 = ndofpernd * conn[5] - 3

        K[pos1:(pos1 + 3), pos1:(pos1 + 3)] += keXY[0:3, 0:3]
        K[pos1:(pos1 + 3), pos2:(pos2 + 3)] += keXY[0:3, 3:6]
        K[pos2:(pos2 + 3), pos1:(pos1 + 3)] += keXY[3:6, 0:3]
        K[pos2:(pos2 + 3), pos2:(pos2 + 3)] += keXY[3:6, 3:6]

    # applied point load vector
    F = np.zeros((ndof, 1))
    for iF in range(f.shape[0]):
        nodeF = Inode[f[iF, 0] == node[:, 0]].item()
        F[((nodeF - 1) * ndofpernd): (nodeF * ndofpernd), 0] = f[iF, 1:4]

    # zero displacement boundary conditions; to improve!
    ifix = np.zeros((ndof, 1))
    for iu in range(ufix.shape[0]):
        nodeufix = Inode[ufix[iu, 0] - 1].item()
        ifix[((nodeufix - 1) * ndofpernd):(nodeufix * ndofpernd), 0] = ufix[iu, 1:4]

    ifix = np.where(ifix != 0)[0]

    K[ifix, :] = 0  # zero out rows
    K[:, ifix] = 0  # zero out cols
    for ii in ifix:
        K[ii, ii] = 1  # put ones on the diagonal

    F[ifix] = 0

    # solve
    U = np.linalg.solve(K, F)

    return U[20 * 3]


# @jit(nopython=True)
def le_frame(x):

    BEAM, NODE, Inode, node, ufix, ne, ndof = init_frame()

    n = x.shape[0]
    u = np.zeros(n)
    for ii in range(n):
        res = solve_frame(x[ii, :], BEAM, NODE, Inode, node, ufix, ne, ndof).item()
        u[ii] = res

    return u
