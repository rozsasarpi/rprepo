#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import copy

import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
import cmocean

from code_rp.gfun import gfun_8, gfun_14, gfun_22, gfun_24, gfun_25, gfun_28, gfun_31, gfun_33, gfun_33, gfun_35, gfun_38,\
   gfun_53, gfun_54, gfun_55, gfun_57,  gfun_60, gfun_63, gfun_75, gfun_77, gfun_89, gfun_91, gfun_107, gfun_110,\
   gfun_111, gfun_201_le_frame, gfun_202_shear_beam, gfun_203_bram_geotech, gfun_213_le_frame, gfun_300_juan_first_passage, gfun_301


# =============================================================================
# pick gfun
# =============================================================================
def select_gfun(reliability_problem_id: np.array):
    if reliability_problem_id == 8:
        gfun = gfun_8.gfun_8
    elif reliability_problem_id == 14:
        gfun = gfun_14.gfun_14
    elif reliability_problem_id == 22:
        gfun = gfun_22.gfun_22
    elif reliability_problem_id == 24:
        gfun = gfun_24.gfun_24
    elif reliability_problem_id == 25:
        gfun = gfun_25.gfun_25
    elif reliability_problem_id == 28:
        gfun = gfun_28.gfun_28
    elif reliability_problem_id == 31:
        gfun = gfun_31.gfun_31
    elif reliability_problem_id == 33:
        gfun = gfun_33.gfun_33
    elif reliability_problem_id == 35:
        gfun = gfun_35.gfun_35
    elif reliability_problem_id == 38:
        gfun = gfun_38.gfun_38
    elif reliability_problem_id == 53:
        gfun = gfun_53.gfun_53
    elif reliability_problem_id == 54:
        gfun = gfun_54.gfun_54
    elif reliability_problem_id == 55:
        gfun = gfun_55.gfun_55
    elif reliability_problem_id == 57:
        gfun = gfun_57.gfun_57
    elif reliability_problem_id == 60:
        gfun = gfun_60.gfun_60
    elif reliability_problem_id == 63:
        gfun = gfun_63.gfun_63
    elif reliability_problem_id == 75:
        gfun = gfun_75.gfun_75
    elif reliability_problem_id == 77:
        gfun = gfun_77.gfun_77
    elif reliability_problem_id == 89:
        gfun = gfun_89.gfun_89
    elif reliability_problem_id == 91:
        gfun = gfun_91.gfun_91
    elif reliability_problem_id == 107:
        gfun = gfun_107.gfun_107
    elif reliability_problem_id == 110:
        gfun = gfun_110.gfun_110
    elif reliability_problem_id == 111:
        gfun = gfun_111.gfun_111
    elif reliability_problem_id == 201:
        gfun = gfun_201_le_frame.gfun_201
    elif reliability_problem_id == 202:
        gfun = gfun_202_shear_beam.gfun_202
    elif reliability_problem_id == 203:
        gfun = gfun_203_bram_geotech.gfun_203
    elif reliability_problem_id == 213:
        gfun = gfun_213_le_frame.gfun_213_le_frame
    elif reliability_problem_id == 300:
        gfun = gfun_300_juan_first_passage.gfun_300
    elif reliability_problem_id == 301:
        gfun = gfun_301.gfun_301
    return gfun


# =============================================================================
# find range
# =============================================================================
def find_support_range(fractile, dist: np.ndarray, mean: np.ndarray, std: np.ndarray) -> np.ndarray:
    """
        Find the range=max-min of the sections of a function to be plotted. Needed to set ylim during visualization.
    """

    lower_f = fractile
    upper_f = 1 - fractile

    n_dimension = len(mean)
    if n_dimension > 10:
        n_dimension = 10
    sup_range = np.zeros((2, n_dimension))

    for ii in range(n_dimension):
        mean_ii = mean[ii]
        std_ii = std[ii]
        var_ii = std_ii**2
        if dist[ii] == 'uniform':
            a = mean_ii - np.sqrt(3 * var_ii)
            b = mean_ii + np.sqrt(3 * var_ii)
            sup_range[:, ii] = scipy.stats.uniform.ppf([lower_f, upper_f], loc=a, scale=b-a)
        elif dist[ii] == 'lognormal':
            loc = np.log(mean_ii ** 2 / np.sqrt(var_ii + mean_ii ** 2))
            scale = np.sqrt(np.log(var_ii / mean_ii ** 2 + 1))
            sup_range[:, ii] = scipy.stats.lognorm.ppf([lower_f, upper_f], s=scale, scale=np.exp(loc))
        elif dist[ii] == 'gumbel_max':
            gamma = np.euler_gamma
            scale = np.sqrt(6) / np.pi * np.sqrt(var_ii)
            loc = mean_ii - scale * gamma
            sup_range[:, ii] = scipy.stats.gumbel_r.ppf([lower_f, upper_f], loc=loc, scale=scale)
        elif dist[ii] == 'exponential':
            rate = 1 / mean_ii
            sup_range[:, ii] = - rate * np.log(1 - np.array([lower_f, upper_f]))
        elif dist[ii] == 'normal':
            sup_range[:, ii] = scipy.stats.norm.ppf([lower_f, upper_f], loc=mean_ii, scale=std_ii)
        else:
            raise ValueError(f'Unknown distribution type: {dist[ii]}')

    return sup_range


def find_z_range(func, fractile, dist: np.ndarray, mean: np.ndarray, std: np.ndarray) -> np.ndarray:
    """
        Find the range=max-min of the sections of a function to be plotted. Needed to set ylim during visualization.
        Kept for historical reasons, integrated into function visualize for efficiency.
    """
    # turn off the upper triangle
    n_dimension = len(mean)
    if n_dimension > 10:
        n_dimension = 10
    # number of devisions along one axis
    n_discr = 100

    sup_range = find_support_range(fractile, dist, mean, std)
    diag = []
    off_diag = []
    for x_i in range(n_dimension):
        for y_i in range(n_dimension):
            # ----------------------------------------------------------------
            # DIAGONAL PLOTS
            # ----------------------------------------------------------------
            if x_i == y_i:
                x_grid = np.linspace(sup_range[0, x_i], sup_range[1, x_i], n_discr)
                xy_grid = np.tile(mean, (n_discr, 1))

                xy_grid[:, x_i] = x_grid
                g = func(xy_grid)[0]
                # TODO: revisit why amin and amax
                # TODO: maybe generalize to component perf functions as well
                diag.append([np.amin(g), np.amax(g)])
            # ----------------------------------------------------------------
            # OFF-DIAGONAL PLOTS
            # ----------------------------------------------------------------
            if x_i > y_i:
                x_grid = np.linspace(sup_range[0, x_i], sup_range[1, x_i], n_discr)
                y_grid = np.linspace(sup_range[0, y_i], sup_range[1, y_i], n_discr)
                X, Y = np.meshgrid(x_grid, y_grid)

                xy_grid = np.tile(mean, (n_discr, n_discr, 1))
                Z = np.zeros((n_discr, n_discr))

                for rows in range(xy_grid.shape[0]):
                    for col in range(xy_grid.shape[1]):
                        xy_grid[rows, col, x_i] = x_grid[rows]
                        xy_grid[rows, col, y_i] = y_grid[col]
                        g = func(xy_grid[rows, col])[0]
                        Z[rows, col] = g

                off_diag.append([np.amin(Z), np.amax(Z)])

    range_diag = [np.amin(np.array(diag)), np.amax(np.array(diag))]
    range_off_diag = [np.amin(np.array(off_diag)), np.amax(np.array(off_diag))]
    range_tot = np.array([min(range_diag[0], range_off_diag[0]), max(range_diag[1], range_off_diag[1])])

    return range_tot


# =============================================================================
# Visualization
# =============================================================================
def visualize(func, fractile, dist: np.ndarray, mean: np.ndarray, std: np.ndarray, gfun: str = "sys", n_discr: int = 100):
    # -----------------------------------------------------------------------------------------------------
    # PRE-PROCESSING
    # -----------------------------------------------------------------------------------------------------
    if not callable(func):
        raise ValueError('A callable function must be provided')

    n_dimension = len(mean)
    n_dimension_limit = 10
    if n_dimension > n_dimension_limit:
        n_dimension = n_dimension_limit
        title = f"Only the first {n_dimension} dimensions (variables) are visualized."
    else:
        title = ""


    # -----------------------------------------------------------------------------------------------------
    # GET SUPPORT RANGE
    # -----------------------------------------------------------------------------------------------------
    sup_range = find_support_range(fractile=fractile, dist=dist, mean=mean, std=std)

    # -----------------------------------------------------------------------------------------------------
    # PLOT
    # -----------------------------------------------------------------------------------------------------
    fig, axes = plt.subplots(n_dimension, n_dimension, figsize=(n_dimension + 3, n_dimension + 3))

    Z_off_diag_all = np.empty((n_discr, n_discr, n_dimension, n_dimension))
    Z_off_diag_all[:] = np.nan
    X_off_diag_all = copy.deepcopy(Z_off_diag_all)
    Y_off_diag_all = copy.deepcopy(Z_off_diag_all)
    Z_diag_all = np.empty((n_discr, n_dimension, n_dimension))
    Z_diag_all[:] = np.nan

    for x_i in range(n_dimension):
        for y_i in range(n_dimension):
            # turn off the upper triangle
            if x_i < y_i:
                axes[x_i, y_i].axis('off')

            # diagonal
            elif x_i == y_i:
                print(f"x_i={x_i+1}/{n_dimension}; \t y_i={y_i+1}/{n_dimension} \t (diagonal)")
                y_labelpad = 15
                x_grid = np.linspace(sup_range[0, x_i], sup_range[1, x_i], n_discr)
                xy_grid = np.tile(mean, (n_discr, 1))

                xy_grid[:, x_i] = x_grid
                g = func(xy_grid)[0].ravel()

                Z_diag_all[:, x_i, y_i] = g

                axes[x_i, x_i].plot(x_grid, g, color='black')

                # left top corner
                if x_i == 0:
                    axes[0, 0].set_xticks([])
                    axes[0, 0].yaxis.tick_right()
                    axes[0, 0].yaxis.set_label_position('right')
                    axes[0, 0].set_ylabel(f'$g_\mathrm{{{gfun}}}$', rotation=0, labelpad=y_labelpad)

                # right bottom corner
                elif x_i == n_dimension - 1:
                    axes[x_i, x_i].set_xlabel(f'$X_{{{n_dimension}}}$')
                    axes[x_i, x_i].yaxis.tick_right()
                    axes[x_i, x_i].yaxis.set_label_position('right')
                    axes[x_i, x_i].set_ylabel(f'$g_\mathrm{{{gfun}}}$', rotation=0, labelpad=y_labelpad)

                # the rest diagonal plot
                else:
                    axes[x_i, x_i].set_xticks([])
                    axes[x_i, x_i].yaxis.tick_right()
                    axes[x_i, x_i].yaxis.set_label_position('right')
                    axes[x_i, x_i].set_ylabel(f'$g_\mathrm{{{gfun}}}$', rotation=0, labelpad=y_labelpad)

            # off diagonal
            else:
                print(f"x_i={x_i+1}/{n_dimension}; \t y_i={y_i+1}/{n_dimension} \t (off-diagonal)")
                y_labelpad = 10
                x_grid = np.linspace(sup_range[0, x_i], sup_range[1, x_i], n_discr)
                y_grid = np.linspace(sup_range[0, y_i], sup_range[1, y_i], n_discr)
                X, Y = np.meshgrid(x_grid, y_grid)

                xy_grid = np.tile(mean, (n_discr, n_discr, 1))
                Z = np.zeros((n_discr, n_discr))

                for rows in range(xy_grid.shape[0]):
                    for col in range(xy_grid.shape[1]):
                        xy_grid[rows, col, x_i] = x_grid[rows]
                        xy_grid[rows, col, y_i] = y_grid[col]
                        g = func(xy_grid[rows, col])[0]
                        Z[rows, col] = g

                Z_off_diag_all[:, :, x_i, y_i] = Z
                X_off_diag_all[:, :, x_i, y_i] = X
                Y_off_diag_all[:, :, x_i, y_i] = Y
                # for the colorbar later
                color = axes[x_i, y_i].contourf(Y, X, Z)

                # left bottom corner
                if x_i == n_dimension - 1 and y_i == 0:
                    axes[x_i, y_i].set_xlabel(f'$X_{1}$')
                    axes[x_i, y_i].set_ylabel(f'$X_{{{n_dimension}}}$', rotation=0, labelpad=y_labelpad)

                # bottom edge x label
                elif x_i == n_dimension - 1:
                    if y_i != 0:
                        axes[x_i, y_i].set_yticks([])
                    axes[x_i, y_i].set_xlabel(f"$X_{{{y_i + 1}}}$")

                # left edge x label
                elif y_i == 0:
                    axes[x_i, y_i].set_xticks([])
                    axes[x_i, y_i].set_ylabel(f"$X_{{{x_i + 1}}}$", rotation=0, labelpad=y_labelpad)

                # rest
                else:
                    axes[x_i, y_i].set_yticks([])
                    axes[x_i, y_i].set_xticks([])

            # Format the axes - for all plots
            axes[x_i, y_i].tick_params(labelsize=6)
            axes[x_i, y_i].xaxis.offsetText.set_fontsize(6)
            axes[x_i, y_i].yaxis.offsetText.set_fontsize(6)
            axes[x_i, y_i].ticklabel_format(scilimits=(-5, 5))

    # -----------------------------------------------------------------------------------------------------
    # SET THE RANGES AND COLORS
    # -----------------------------------------------------------------------------------------------------
    # get the range of the ordinate
    range_tot = [np.nanmin([np.nanmin(Z_diag_all), np.nanmin(Z_off_diag_all)]),
                 np.nanmax([np.nanmax(Z_diag_all), np.nanmax(Z_off_diag_all)])]

    levels = np.linspace(signif_floor(range_tot[0]), signif_ceil(range_tot[1]), 10)
    # padding
    r = range_tot[1] - range_tot[0]
    d = 0.05
    range_tot = [range_tot[0] - d * r, range_tot[1] + d * r]

    # Set axis ranges and colors
    for x_i in range(n_dimension):
        for y_i in range(n_dimension):
            # turn off the upper triangle
            if x_i < y_i:
                pass  # do nothing

            # diagonal
            elif x_i == y_i:
                axes[x_i, x_i].set_ylim([range_tot[0], range_tot[1]])

            # off diagonal
            else:
                color = axes[x_i, y_i].contourf(Y_off_diag_all[:, :, x_i, y_i],
                                                X_off_diag_all[:, :, x_i, y_i],
                                                Z_off_diag_all[:, :, x_i, y_i],
                                                levels=levels,
                                                cmap=cmocean.cm.deep_r)

    fig.suptitle(title)
    # -----------------------------------------------------------------------------------------------------
    # COLORBAR
    # -----------------------------------------------------------------------------------------------------
    cax = plt.axes([0.85, 0.55, 0.025, 0.3])
    cb = fig.colorbar(color, cax=cax)
    cb.ax.set_ylabel(f'$g_\mathrm{{{gfun}}}$', rotation=0, labelpad=y_labelpad)
    cb.ax.tick_params(labelsize=6)
    cb.ax.yaxis.get_offset_text().set_fontsize(6)
    cb.formatter.set_powerlimits((-5, 5))
    cb.update_ticks()
    return fig


def signif_ceil(x, digits: int = 3):
    """
        x = sci_coeff * 10**sci_exp
    """
    x = np.array([x])
    # scientific notation
    sci_exp = np.array([int(np.floor(np.log10(np.abs(x))))])
    sci_scale = 10.**(sci_exp)
    sci_coeff = x / sci_scale

    # number of digits
    dig_scale = 10.**digits

    sx = np.ceil(sci_coeff * dig_scale) * sci_scale / dig_scale
    return sx


def signif_floor(x, digits: int = 3):
    """
        x = sci_coeff * 10**sci_exp
    """
    x = np.array([x])
    # scientific notation
    sci_exp = np.array([int(np.floor(np.log10(np.abs(x))))])
    sci_scale = 10.**(sci_exp)
    sci_coeff = x / sci_scale

    # number of digits
    dig_scale = 10.**digits

    sx = np.floor(sci_coeff * dig_scale) * sci_scale / dig_scale
    return sx
