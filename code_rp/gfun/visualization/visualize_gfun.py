import os
import pandas as pd

from code_rp.gfun.visualization.utils_visualization_giuli import *

# ==================================================================================
#
# ==================================================================================
BASE_DIR = os.getcwd()
CODE_DIR = os.path.join(BASE_DIR, "code_rp")
MISC_DIR = os.path.join(CODE_DIR, "misc")
GFUN_DIR = os.path.join(CODE_DIR, "gfun")
VISUAL_DIR = os.path.join(GFUN_DIR, "visualization")
PLOT_DIR = os.path.join(BASE_DIR, "docs", "_static", "gfun_matrix_plot")

file_name = 'probabilistic_models_up.csv'

fractile = 0.01
# ==================================================================================
#
# ==================================================================================
pm = pd.read_csv(os.path.join(MISC_DIR, file_name), index_col=False)

pm_unique = pm[['set_id', 'problem_id', 'reliability_problem_id']].drop_duplicates().reset_index(drop=True)

idx = pm_unique['reliability_problem_id'].isin([201, 202, 203, 213, 301])
# idx = pm_unique['reliability_problem_id'].isin([203])
pm_unique = pm_unique[idx].reset_index(drop=True)

# idx = pm_unique['reliability_problem_id'].isin([111])
# pm_unique = pm_unique[idx].reset_index(drop=True)

for ii in range(pm_unique.shape[0]):
    set_id = pm_unique['set_id'][ii]
    problem_id = pm_unique['problem_id'][ii]
    rp_id = pm_unique['reliability_problem_id'][ii]
    # set_id = -1
    # problem_id = 2
    print("="*50)
    print(f"set_id={set_id}; problem_id={problem_id}; rp_id={rp_id}")
    print("Running...")

    aa = pm[(pm["set_id"] == set_id) & (pm["problem_id"] == problem_id)]
    aa = aa.reset_index()
    dim = aa.shape[0]

    reliability_problem_id = np.array(aa.loc[0, 'reliability_problem_id'])
    fun = select_gfun(reliability_problem_id)

    dist = np.array(aa.loc[0:dim, 'distribution_type'])
    mean = np.array(aa.loc[0:dim, 'mean'])
    std = np.array(aa.loc[0:dim, 'std'])

    if dim < 8:
        n_discr = 100
    else:
        n_discr = 20

    plot = visualize(fun, fractile, dist, mean=mean, std=std, n_discr=n_discr)
    plot_name = os.path.join(PLOT_DIR, f"rp_{reliability_problem_id}_matrix.png")
    plt.savefig(plot_name, transparent=True, bbox_inches="tight")
    print("Complete.")
